# LibreLinguist

**Version 0.8.0** - [Change log](CHANGELOG.md)

LibreLinguist is a free and open source language learning tool.

## Setting up a dev environment

**N.B. The instructions below are for setting up a development environent. Some things will need to be adjusted for a production deployment.**

LibreLinguist consists of a Python back-end and a React front-end. To get set up, you'll need to have docker and node installed.

**Before you start**

LibreLinguist expects to find secrets such as database credentials in a file called `docker/python/config.ini`. Please copy or rename `docker/python/example-config.ini` and amend the entries starting `CHANGEME` to the credentials you wish to use.

Similarly, `docker/example-docker-compose.yaml` should be copied or renamed to `docker/docker-compose.yaml` and edited to include the correct database credentials.

**Starting the containers**

The backend code is in the `docker` directory and consists of a Python container and a PostgreSQL database container. To start up the containers, `cd` into the directory and enter the command `docker compose up -d`. It may be necessary to stop the containers and re-run `docker compose up -d` due to the postgres container needing to create the database the first time it runs (but hopefully the `restart: on-failure` in docker-compose.yaml has fixed this).

**Starting the React app**
The React app is in the `frontend` directory. To install, `cd` into this directory and type `npm install`. To start, type `npm start`. You can also refer to the [README](frontend/README.md) in the same directory.

## Testing the API

The backend implements an API using fastapi. You can use the built-in Swagger UI for testing endpoints - the URL for this is [http://localhost:8080/docs](http://localhost:8080/docs).

## Known issues / bugs
- For the French verb test, there's no gender hint for "they" - you have to guess between "ils" and "elles".
