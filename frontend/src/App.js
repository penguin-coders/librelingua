/**************************************************************************
 * LibreLinguist - a free and open source language learning tool          *
 * Copyright (C) 2022-2023 Mark Williams                                  *
 *                                                                        *
 * This file is part of LibreLinguist.                                    *
 *                                                                        *
 * LibreLinguist is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * LibreLinguist is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 * GNU General Public License for more details.                           *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

import { useContext, useState } from 'react'

import Header from './components/Header.js'
import Test from './components/Test.js'
import LoginRegister from './components/LoginRegister'
import { UserContext } from './context/UserContext'
import Loading from './components/Loading'
import SelectContext from './components/SelectContext'

const App = () => {

	const [token] = useContext(UserContext)
	const [,,language, setLanguage] = useContext(UserContext)
	const [,,,,ctxt, setCtxt] = useContext(UserContext)
	const [loading, setLoading] = useState(false)
	const [selectCtxt, setSelectCtxt] = useState(false)

	// Set user language
	const switchLanguage = async (lang) => {
		setLoading(true)
		setLanguage({...language, 'id':lang})
		setSelectCtxt(false)
      const requestOptions = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
		const res = await fetch(`/setlang?lang=${lang}`,requestOptions)
		const newlang = await res.json()
		setLanguage(newlang)
		requestOptions.method = "GET"
      const ctxt_res = await fetch("/context", requestOptions);
		const newctxt = await ctxt_res.json()
		setCtxt(newctxt)
		setLoading(false)
	}

 	return (
	<div className="App">
		<Header language={language} onLangChange={switchLanguage} setSelCtxt={setSelectCtxt} />
		<div className='container'>
			{!token ? 
				(<LoginRegister loading={loading} setLoading={setLoading} />)
			:
				loading ?
				(<Loading />)
				: 
					selectCtxt ?
					(<SelectContext setSelCtxt={setSelectCtxt} setCtxt={setCtxt} lang={language} />)
					:
					language != "" && 
						(<Test language={language} ctxt={ctxt} />)
		}
		</div>
	</div>
	)
}

export default App
