/**************************************************************************
 * LibreLinguist - a free and open source language learning tool          *
 * Copyright (C) 2022-2023 Mark Williams                                  *
 *                                                                        *
 * This file is part of LibreLinguist.                                    *
 *                                                                        *
 * LibreLinguist is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * LibreLinguist is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 * GNU General Public License for more details.                           *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

import { useState } from 'react'
import Login from './Login'
import Register from './Register'
import Button from './Button'
import Loading from './Loading'
import A from './A'

const LoginRegister = ({ loading, setLoading }) => {

	const [register, setRegister] = useState(false);
	const toggleReg = () => { setRegister(!register) }

	return (
		<>
			{loading ?
				(<Loading />)
			: !register ? (
				<>
				<Login setLoading={setLoading} />
				<div>
				<p><b>Not got an account?</b></p>
				<p><A text='Create an account' onClick={toggleReg} /></p>
				</div>
				</>
			) : (
				<>
				<Register setLoading={setLoading} />
				<div>
					<p><b>Already got an account?</b></p>
					<p><A text='Login to your account' onClick={toggleReg} /></p>
				</div>
				</>
			)}
		</>)
}

export default LoginRegister
