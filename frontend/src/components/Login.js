/**************************************************************************
 * LibreLinguist - a free and open source language learning tool          *
 * Copyright (C) 2022-2023 Mark Williams                                  *
 *                                                                        *
 * This file is part of LibreLinguist.                                    *
 *                                                                        *
 * LibreLinguist is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * LibreLinguist is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 * GNU General Public License for more details.                           *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

import React, { useState, useContext } from "react";
import ErrorMessage from "./ErrorMessage";
import Button from './Button'
import { UserContext } from "../context/UserContext";

const Login = ({ setLoading }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [, setToken] = useContext(UserContext);

  const submitLogin = async () => {
	setLoading(true)
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: JSON.stringify(
        `grant_type=&username=${email}&password=${password}&scope=&client_id=&client_secret=`
      ),
    };

    const response = await fetch("/user/login", requestOptions);
    const data = await response.json();

    if (!response.ok) {
      setErrorMessage(data.detail);
    } else {
      setToken(data.access_token);
    }
	setLoading(false)
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    submitLogin();
  };

  return (
	  <>
      <h2>Login</h2>
      <form className="box" onSubmit={handleSubmit}>
			<div className='form-control'>
          <label htmlFor='email'>Email Address</label>
            <input
	  				id='email'
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
        </div>
			<div className='form-control'>
          <label htmlFor='pass'>Password</label>
            <input
	  				id='pass'
              type="password"
              placeholder="Enter password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
        </div>
        <ErrorMessage message={errorMessage} />
        <Button text='Login' onClick={handleSubmit} />
      </form>
	  </>
  );
};

export default Login;
