/*************************************************************************
 * LibreLinguist - a free and open source language learning tool           *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of LibreLinguist.                                     *
 *                                                                       *
 * LibreLinguist is free software: you can redistribute it and/or modify   *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * LibreLinguist is distributed in the hope that it will be useful,        *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with LibreLinguist.  If not, see <https://www.gnu.org/licenses/>. *
 *************************************************************************/

import { useContext, useEffect, useState } from 'react'
import { UserContext } from '../context/UserContext'
import A from './A'

const SelectContext = ({setSelCtxt, setCtxt}) => {
	const [token] = useContext(UserContext)
	const [ctxtList, setCtxtList] = useState([])

	useEffect(() => {
		getContextList()
	}, [])
	
	// Get list of contexts
	const getContextList = async () => {
      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
		const res = await fetch("/contextlist",requestOptions)
		const cl = await res.json()
		setCtxtList(cl)
	}
	
	const selectCtxt = async (context_id) => { 
      const requestOptions = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
		const res = await fetch(`/setcontext?ctxt=${context_id}`,requestOptions)
		const newctxt = await res.json()
		setCtxt(newctxt)
		setSelCtxt(false) 
	}

	return (
		<>
		{ctxtList.map((ctxt) => {
			return (
				<>
				<A text={ctxt.context_name} onClick={() => selectCtxt(ctxt.id)} /><br />
				</>
			)
		})}
		</>
	)
}

export default SelectContext
