/**************************************************************************
 * LibreLinguist - a free and open source language learning tool          *
 * Copyright (C) 2022-2023 Mark Williams                                  *
 *                                                                        *
 * This file is part of LibreLinguist.                                    *
 *                                                                        *
 * LibreLinguist is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * LibreLinguist is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 * GNU General Public License for more details.                           *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

import { useContext } from 'react'
import { UserContext } from '../context/UserContext'
import Button from './Button'

const Header = ({ language, onLangChange, setSelCtxt }) => {

	const [token] = useContext(UserContext)

	const selCtxt = () => {setSelCtxt(true)}

	return (
		<div className='header'>
		<h1>LibreLinguist</h1>
		{token && language != '' ? (
		<>
		<Button text='Select Context' onClick={selCtxt} />
		<form>
			<div className='form-control-check'>
				<label><input type='radio' name='language' value='2' onChange={(e) => onLangChange(e.target.value)} checked={`${(language.id=='2')?'checked':''}`} /> Polish</label>
			</div>
			<div className='form-control-check'>
				<label><input type='radio' name='language' value='3' onChange={(e) => onLangChange(e.target.value)} checked={`${(language.id=='3')?'checked':''}`}/> French</label>
			</div>
		</form>
		</>
		) : (<></>)}
		</div>
	)
}

export default Header
