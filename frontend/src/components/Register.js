/*************************************************************************
 * LibreLinguist - a free and open source language learning tool           *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of LibreLinguist.                                     *
 *                                                                       *
 * LibreLinguist is free software: you can redistribute it and/or modify   *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * LibreLinguist is distributed in the hope that it will be useful,        *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with LibreLinguist.  If not, see <https://www.gnu.org/licenses/>. *
 *************************************************************************/

import React, { useContext, useState } from "react";
import Button from './Button'

import { UserContext } from "../context/UserContext";
import ErrorMessage from "./ErrorMessage";

const Register = ({ setLoading }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmationPassword, setConfirmationPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [, setToken] = useContext(UserContext);

  const submitRegistration = async () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email: email, passwd: password, lang: 3 }),
    };

    const response = await fetch("/user/register", requestOptions);
    const data = await response.json();

    if (!response.ok) {
      setErrorMessage(data.detail);
    } else {
      setToken(data.access_token);
    }
  };

  const handleSubmit = (e) => {
	setLoading(true)
    e.preventDefault();
    if (password === confirmationPassword && password.length > 5) {
      submitRegistration();
    } else {
      setErrorMessage(
        "Ensure that the passwords match and greater than 5 characters"
      );
    }
	setLoading(false)
  };

  return (
    <>
      <h2>Create an account</h2>
      <form className="box" onSubmit={handleSubmit}>
			<div className='form-control'>
          <label className="label">Email Address</label>
            <input
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="input"
              required
            />
        </div>
			<div className='form-control'>
          <label className="label">Password</label>
            <input
              type="password"
              placeholder="Enter password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="input"
              required
            />
        </div>
			<div className='form-control'>
          <label className="label">Confirm Password</label>
            <input
              type="password"
              placeholder="Enter password"
              value={confirmationPassword}
              onChange={(e) => setConfirmationPassword(e.target.value)}
              className="input"
              required
            />
        </div>
        <ErrorMessage message={errorMessage} />
        <Button text='Register' onClick={handleSubmit} />
      </form>
    </>
  );
};

export default Register;
