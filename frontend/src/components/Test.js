/*************************************************************************
 * LibreLinguist - a free and open source language learning tool           *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of LibreLinguist.                                     *
 *                                                                       *
 * LibreLinguist is free software: you can redistribute it and/or modify   *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * LibreLinguist is distributed in the hope that it will be useful,        *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with LibreLinguist.  If not, see <https://www.gnu.org/licenses/>. *
 *************************************************************************/

import { useState, useEffect, useRef, useContext } from 'react'
import Button from './Button'
import { UserContext } from '../context/UserContext'

const Test = ({ language, ctxt }) => {

	const [question, setQuestion] = useState('')
	const [disambiguation, setDisambiguation] = useState('')
	const [correctAnswer, setCorrectAnswer] = useState('')
	const [userAnswer, setUserAnswer] = useState('')
	const [showTestForm, setShowTestForm] = useState(false)
	const [token] = useContext(UserContext)
	
	const answerRef = useRef(null)
	const continueRef = useRef(null)

	useEffect(() => {
		getTestItem()
		setShowTestForm(true)
	}, [])

	useEffect(() => {
		answerRef.current && answerRef.current.focus()
	}, [question]) 
	
	useEffect(() => {
		continueRef.current && continueRef.current.focus()
	}, [showTestForm]) 

	// Get testitem
	const getTestItem = async () => {
      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
		const res = await fetch('/testitem',requestOptions)
		const testItem = await res.json()
		setQuestion(testItem.question)
		setCorrectAnswer(testItem.answer)
		setDisambiguation(testItem.disambiguation)
	}

	// Check answer
	const checkAnswer = (e) => {
		e.preventDefault()
		setShowTestForm(false)
	}

	// Continue to next test item
	const nextTestItem = (e) => {
		e.preventDefault()
		setUserAnswer('')
		getTestItem()
		setShowTestForm(true)
	}

	// Update test when language changes
	useEffect(() => {
		getTestItem()
	},[language])

	return (
		<>
		<h2>{ctxt.context_name}</h2>
		{showTestForm && <form>
			<div className='form-control'>
				<label htmlFor='answer'>{question} {disambiguation}</label>
				<input 
					type='text' 
					name='answer' 
					id='answer' 
					value={userAnswer}  
					placeholder={`Type in ${language.name}...`}
					onChange={(e) => setUserAnswer(e.target.value)}
					ref={answerRef}
					autocomplete='off'
				/>
			</div>
			<Button text='Submit' onClick={checkAnswer}/>
			<Button text='Skip' onClick={nextTestItem} />
		</form>}
		{!showTestForm && <form>
			<div className='form-control'>
			{userAnswer===correctAnswer ? (
					<>
						<p>Correct!</p>
						<p>The answer is:<br />
							<span className='correct'>{correctAnswer}</span>
						</p>
					</>
				) : (
					<>
						<p>Sorry, <span className='incorrect'>"{userAnswer}"</span> is not correct.</p>
						<p>The correct answer is:<br />
							<span className='correct'>{correctAnswer}</span>
						</p>
					</>
				)
			}

			</div>
			<Button text='Continue' onClick={nextTestItem} buttonRef={continueRef} />
		</form>}
		</>
	)
}

export default Test
