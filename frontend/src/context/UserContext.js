/**************************************************************************
 * LibreLinguist - a free and open source language learning tool          *
 * Copyright (C) 2022 Mark Williams                                       *
 *                                                                        *
 * This file is part of LibreLinguist.                                    *
 *                                                                        *
 * LibreLinguist is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * LibreLinguist is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 * GNU General Public License for more details.                           *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. *
 *************************************************************************/

import React, { createContext, useEffect, useState } from "react";

export const UserContext = createContext();

export const UserProvider = (props) => {
	const [token, setToken] = useState(localStorage.getItem("libreLinguistToken")); 
	const [language, setLanguage] = useState("")
	const [ctxt, setCtxt] = useState("")

  useEffect(() => {
    const getLanguage = async () => {
      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      };

      const res = await fetch("/language", requestOptions);
      const ctxt_res = await fetch("/context", requestOptions);

      if (!res.ok) {
        setToken(null);
      } else {
			const lang = await res.json()
			const ctext = await ctxt_res.json()
			console.log(ctext)
			setLanguage(lang)
			setCtxt(ctext)
		}
      localStorage.setItem("libreLinguistToken", token);
    };
    getLanguage();
  }, [token]);

  return (
    <UserContext.Provider value={[token, setToken, language, setLanguage, ctxt, setCtxt]}>
      {props.children}
    </UserContext.Provider>
  );
};
