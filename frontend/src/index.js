/**************************************************************************
 * LibreLinguist - a free and open source language learning tool          *
 * Copyright (C) 2022-2023 Mark Williams                                  *
 *                                                                        *
 * This file is part of LibreLinguist.                                    *
 *                                                                        *
 * LibreLinguist is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * LibreLinguist is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 * GNU General Public License for more details.                           *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { UserProvider } from './context/UserContext'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<UserProvider>
    <App />
	</UserProvider>
);
