-- 
-- depends: 20240624_01_6aLf9

INSERT INTO public.contexts (id,ntv_lang_id,tgt_lang_id,context_name) VALUES
	 (3,1,2,'Polish verbs & pronouns - present tense'),
	 (4,1,2,'Saying you like things');
INSERT INTO public.noun_declensions (id,language_id,declension,endings) VALUES
	 (1,2,8,'{"1": {"acc": "-o", "dat": "-u", "gen": "-a", "ins": "-iem", "loc": "-u", "nom": "-o", "voc": "-o"}, "2": {"acc": "-a", "dat": "-om", "gen": "-", "ins": "-ami", "loc": "-ach", "nom": "-a", "voc": "-a"}}'),
	 (3,2,4,'{"1": {"acc": "-a", "dat": "-u", "gen": "-a", "ins": "-em", "loc": "-e", "nom": "-", "voc": "-e"}, "2": {"acc": "-y", "dat": "-om", "gen": "-ów", "ins": "-ami", "loc": "-ach", "nom": "-y", "voc": "-y"}}'),
	 (2,1,0,'{"1": {"gen": "-''s", "nom": "-"}, "2": {"gen": "-s''", "nom": "-s"}}');
INSERT INTO public.noun_translations (id,lang1,lang2) VALUES
	 (1,2,1);
INSERT INTO public.nouns (id,language_id,lemma,irregularities,declension) VALUES
	 (2,1,'apple','{}',0),
	 (1,2,'jabłko','{"2": {"gen": "jabłek"}}',8),
	 (3,1,'banana','{}',0),
	 (4,2,'banan','{"1": {"dat": "-owi", "loc": "-ie", "voc": "-ie"}}',4);
INSERT INTO public.phrases (id,tgt_words,ntv_words,translations) VALUES
	 (3,'[6, 2]','[4, 1]','[[0, 0], [1, 1]]'),
	 (4,'[8]','[7]','[[0, 0]]'),
	 (5,'[2, 8]','[4, 10, 9]','[[1, 0], [2, 1]]'),
	 (6,'[2, 11]','[4, 10, 12]','[[1, 0], [2, 1]]');
INSERT INTO public.sentences (id,ntv_word_order,tgt_word_order,context_id,phrases) VALUES
	 (3,'[[0, 0], [0, 1]]','[[0, 0], [0, 1]]',3,'[3]'),
	 (4,'[[0, 0]]','[[0, 0]]',1,'[4]'),
	 (5,'[[0, 0], [0, 1], [0, 2]]','[[0, 0], [0, 1]]',4,'[5]'),
	 (6,'[[0, 0], [0, 1], [0, 2]]','[[0, 0], [0, 1]]',4,'[6]');
INSERT INTO public.words (id,part_of_speech,word_ids,lang,fixed_properties) VALUES
	 (6,11,'[]',2,'{}'),
	 (7,16,'[]',1,'{"verb_form": "infinitive"}'),
	 (8,16,'[]',2,'{"verb_form": "infinitive"}'),
	 (10,16,'[28, 29]',1,'{}'),
	 (9,16,'[6, 10, 12, 14, 16, 30]',1,'{"verb_form": "infinitive"}'),
	 (11,8,'[]',2,'{"case": "acc", "number": 2}'),
	 (12,8,'[]',1,'{"number": 2}');
