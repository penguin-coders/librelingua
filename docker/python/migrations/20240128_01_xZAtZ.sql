-- 
-- depends: 20230222_01_gZRW9

UPDATE public.verbs SET irregularities='{"stem": "ma"}'::jsonb WHERE id=7;
UPDATE public.verbs SET irregularities='{"stem": "pisz"}'::jsonb WHERE id=9;
UPDATE public.verbs SET irregularities='{"16": "-dzą", "stem": "je"}'::jsonb WHERE id=27;
UPDATE public.verbs SET irregularities='{"5": "-es"}'::jsonb WHERE id=2;
UPDATE public.verbs SET irregularities='{"5": "has"}'::jsonb WHERE id=8;
UPDATE public.verbs SET irregularities='{"9": "-em", "10": "-eśmy", "11": "-eś", "12": "-eście", "16": "są", "stem": "jest"}'::jsonb WHERE id=17;
UPDATE public.verbs SET irregularities='{"1": "am", "5": "is", "stem": "are"}'::jsonb WHERE id=18;

UPDATE public.verb_conjugations SET endings='{"1": "-", "2": "-", "3": "-", "4": "-", "5": "-s", "8": "-"}'::jsonb WHERE id=1;
UPDATE public.verb_conjugations SET endings='{"9": "-", "10": "-", "11": "-", "12": "-", "13": "-", "16": "-"}'::jsonb WHERE id=2;
UPDATE public.verb_conjugations SET endings='{"9": "-ę", "10": "-emy", "11": "-esz", "12": "-ecie", "13": "-e", "16": "-ą"}'::jsonb WHERE id=3;
UPDATE public.verb_conjugations SET endings='{"9": "-ę", "10": "-my", "11": "-sz", "12": "-cie", "13": "-", "16": "-ą"}'::jsonb WHERE id=4;
UPDATE public.verb_conjugations SET endings='{"9": "-m", "10": "-my", "11": "-sz", "12": "-cie", "13": "-", "16": "-ją"}'::jsonb WHERE id=5;
UPDATE public.verb_conjugations SET endings='{"19": "-e", "20": "-es", "21": "-e", "23": "-ons", "24": "-ez", "25": "-ent"}'::jsonb WHERE id=6;
UPDATE public.verb_conjugations SET endings='{"19": "-is", "20": "-is", "21": "-it", "23": "-issons", "24": "-issez", "25": "-issent"}'::jsonb WHERE id=7;
UPDATE public.verb_conjugations SET endings='{"19": "-s", "20": "-s", "21": "-", "23": "-ons", "24": "-ez", "25": "-ent"}'::jsonb WHERE id=8;
