-- 
-- depends: 20240624_02_LMTJM

CREATE OR REPLACE VIEW public.vw_nt_nouns
AS SELECT l1.code AS n1_lang,
    n1.id AS n1_id,
    n1.lemma AS n1_lemma,
    n1.irregularities AS n1_irregularities,
    n1.declension AS n1_declension,
    l2.code AS n2_lang,
    n2.id AS n2_id,
    n2.lemma AS n2_lemma,
    n2.irregularities AS n2_irregularities,
    n2.declension AS n2_declension
   FROM nouns n1
     JOIN vw_bidirectional_noun_trans vt ON n1.id = vt.lng1
     JOIN nouns n2 ON n2.id = vt.lng2
     JOIN languages l1 ON l1.id = n1.language_id
     JOIN languages l2 ON l2.id = n2.language_id;

INSERT INTO public.noun_translations (id, lang1, lang2) VALUES(2, 3, 4);

SELECT pg_catalog.setval('public.contexts_id_seq', 4, true);
SELECT pg_catalog.setval('public.languages_id_seq', 3, true);
SELECT pg_catalog.setval('public.noun_declensions_id_seq', 5, true);
SELECT pg_catalog.setval('public.nouns_id_seq', 4, true);
SELECT pg_catalog.setval('public.noun_translations_id_seq', 2, true);
SELECT pg_catalog.setval('public.parts_of_speech_id_seq', 17, false);
SELECT pg_catalog.setval('public.phrases_id_seq', 6, true);
SELECT pg_catalog.setval('public.pronouns_id_seq', 26, true);
SELECT pg_catalog.setval('public.sentences_id_seq', 6, true);
SELECT pg_catalog.setval('public.user_contexts_id_seq', 1, true);
SELECT pg_catalog.setval('public.user_languages_id_seq', 1, true);
SELECT pg_catalog.setval('public.users_id_seq', 1, true);
SELECT pg_catalog.setval('public.verb_conjugations_id_seq', 8, true);
SELECT pg_catalog.setval('public.verb_regexes_id_seq', 4, true);
SELECT pg_catalog.setval('public.verbs_id_seq', 30, true);
SELECT pg_catalog.setval('public.verb_translations_id_seq', 15, true);
SELECT pg_catalog.setval('public.words_id_seq', 12, true);
