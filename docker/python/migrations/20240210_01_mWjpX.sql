-- 
-- depends: 20240131_01_4fGyF

--
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);
--
-- Name: user_contexts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_contexts (
    id integer NOT NULL,
    user_id integer NOT NULL,
    tgt_lang_id integer NOT NULL,
    context_id integer NOT NULL
);


ALTER TABLE public.user_contexts OWNER TO postgres;

--
-- Name: user_contexts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_contexts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.user_contexts_id_seq OWNER TO postgres;

--
-- Name: user_contexts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_contexts_id_seq OWNED BY public.user_contexts.id;

--
-- Name: user_contexts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contexts ALTER COLUMN id SET DEFAULT nextval('public.user_contexts_id_seq'::regclass);

--
-- Name: user_contexts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_contexts_id_seq', 2, true);

--
-- Name: user_contexts user_contexts_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contexts
    ADD CONSTRAINT user_contexts_pk PRIMARY KEY (id);

--
-- Name: user_contexts user_contexts_ctxt_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contexts
    ADD CONSTRAINT user_contexts_ctxt_fk FOREIGN KEY (context_id) REFERENCES public.contexts(id);

--
-- Name: user_contexts user_contexts_lang_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contexts
    ADD CONSTRAINT user_contexts_lang_fk FOREIGN KEY (tgt_lang_id) REFERENCES public.languages(id);

--
-- Name: user_contexts user_contexts_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contexts
    ADD CONSTRAINT user_contexts_user_fk FOREIGN KEY (user_id) REFERENCES public.users(id);

