-- 
-- depends: 20240210_01_mWjpX

UPDATE public.words SET lang=1 WHERE id=1;
UPDATE public.words SET lang=2 WHERE id=2;
UPDATE public.words SET lang=3 WHERE id=3;

ALTER TABLE public.words ADD fixed_properties jsonb NOT NULL DEFAULT '{}';


--
-- Name: noun_declensions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.noun_declensions (
    id integer NOT NULL,
    language_id integer NOT NULL,
    declension smallint NOT NULL,
    endings jsonb NOT NULL
);


ALTER TABLE public.noun_declensions OWNER TO postgres;

--
-- Name: noun_declensions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.noun_declensions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.noun_declensions_id_seq OWNER TO postgres;

--
-- Name: noun_declensions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.noun_declensions_id_seq OWNED BY public.noun_declensions.id;


--
-- Name: noun_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.noun_translations (
    id integer NOT NULL,
    lang1 integer NOT NULL,
    lang2 integer NOT NULL
);


ALTER TABLE public.noun_translations OWNER TO postgres;

--
-- Name: noun_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.noun_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.noun_translations_id_seq OWNER TO postgres;

--
-- Name: noun_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.noun_translations_id_seq OWNED BY public.noun_translations.id;


--
-- Name: nouns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nouns (
    id integer NOT NULL,
    language_id integer NOT NULL,
    lemma character varying NOT NULL,
    irregularities jsonb DEFAULT '{}'::jsonb NOT NULL,
    declension integer NOT NULL
);


ALTER TABLE public.nouns OWNER TO postgres;

--
-- Name: nouns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nouns_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nouns_id_seq OWNER TO postgres;

--
-- Name: nouns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nouns_id_seq OWNED BY public.nouns.id;

--
-- Name: noun_declensions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_declensions ALTER COLUMN id SET DEFAULT nextval('public.noun_declensions_id_seq'::regclass);


--
-- Name: noun_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_translations ALTER COLUMN id SET DEFAULT nextval('public.noun_translations_id_seq'::regclass);


--
-- Name: nouns id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nouns ALTER COLUMN id SET DEFAULT nextval('public.nouns_id_seq'::regclass);

-- public.vw_bidirectional_noun_trans source

CREATE OR REPLACE VIEW public.vw_bidirectional_noun_trans
AS SELECT noun_translations.lang1 AS lng1,
    noun_translations.lang2 AS lng2
   FROM noun_translations
UNION
 SELECT noun_translations.lang2 AS lng1,
    noun_translations.lang1 AS lng2
   FROM noun_translations;

-- public.vw_nt_nouns source

CREATE OR REPLACE VIEW public.vw_nt_nouns
AS SELECT l1.code AS n1_lang,
    n1.id AS n1_id,
    n1.lemma AS n1_lemma,
    n1.irregularities AS n1_irregularities,
    n1.declension AS n1_declension,
    l2.code AS n2_lang,
    n2.id AS n2_id,
    n2.lemma AS n2_lemma,
    n2.irregularities AS n2_irregularities,
    n2.declension AS n2_declension
   FROM nouns n1
     JOIN vw_bidirectional_verb_trans vt ON n1.id = vt.lng1
     JOIN nouns n2 ON n2.id = vt.lng2
     JOIN languages l1 ON l1.id = n1.language_id
     JOIN languages l2 ON l2.id = n2.language_id;

-- public.vw_vt_verbs source

CREATE OR REPLACE VIEW public.vw_vt_verbs
AS SELECT l1.code AS v1_lang,
    v1.infinitive AS v1_infinitive,
    v1.conjugation AS v1_conjugation,
    v1.irregularities AS v1_irregularities,
    l2.code AS v2_lang,
    v2.infinitive AS v2_infinitive,
    v2.conjugation AS v2_conjugation,
    v2.irregularities AS v2_irregularities,
    v1.id AS v1_id,
    v2.id AS v2_id
   FROM verbs v1
     JOIN vw_bidirectional_verb_trans vt ON v1.id = vt.lng1
     JOIN verbs v2 ON v2.id = vt.lng2
     JOIN languages l1 ON l1.id = v1.language_id
     JOIN languages l2 ON l2.id = v2.language_id;

--
-- Name: noun_declensions noun_declensions_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_declensions
    ADD CONSTRAINT noun_declensions_pk PRIMARY KEY (id);


--
-- Name: noun_translations noun_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_translations
    ADD CONSTRAINT noun_translations_pkey PRIMARY KEY (id);


--
-- Name: nouns nouns_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nouns
    ADD CONSTRAINT nouns_pk PRIMARY KEY (id);

--
-- Name: noun_translations noun_translations_lang1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_translations
    ADD CONSTRAINT noun_translations_lang1_fkey FOREIGN KEY (lang1) REFERENCES public.verbs(id);


--
-- Name: noun_translations noun_translations_lang2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_translations
    ADD CONSTRAINT noun_translations_lang2_fkey FOREIGN KEY (lang2) REFERENCES public.verbs(id);

