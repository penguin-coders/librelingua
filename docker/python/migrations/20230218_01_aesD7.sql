-- 
-- depends: 20230212_02_Z1CXO


--
-- Name: verb_regexes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.verb_regexes (
    id integer NOT NULL,
    language_id integer NOT NULL,
    sequence integer NOT NULL,
    property character varying(10) NOT NULL,
    pattern character varying(20) NOT NULL,
    replacement character varying(10) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.verb_regexes OWNER TO postgres;

--
-- Name: verb_regexes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.verb_regexes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.verb_regexes_id_seq OWNER TO postgres;

--
-- Name: verb_regexes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.verb_regexes_id_seq OWNED BY public.verb_regexes.id;

--
-- Name: vw_pronouns; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vw_pronouns AS
 SELECT p.id,
    p.language_id,
    p.pronoun,
    p.person,
    p.number,
    1 AS gender
   FROM public.pronouns p
  WHERE (p.gender = '-1'::integer)
UNION
 SELECT p.id,
    p.language_id,
    p.pronoun,
    p.person,
    p.number,
    2 AS gender
   FROM public.pronouns p
  WHERE (p.gender = '-1'::integer)
UNION
 SELECT p.id,
    p.language_id,
    p.pronoun,
    p.person,
    p.number,
    3 AS gender
   FROM public.pronouns p
  WHERE (p.gender = '-1'::integer)
UNION
 SELECT p.id,
    p.language_id,
    p.pronoun,
    p.person,
    p.number,
    p.gender
   FROM public.pronouns p
  WHERE (p.gender > 0);


ALTER TABLE public.vw_pronouns OWNER TO postgres;

--
-- Name: verb_regexes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_regexes ALTER COLUMN id SET DEFAULT nextval('public.verb_regexes_id_seq'::regclass);


INSERT INTO public.languages (id, "name", code) VALUES(3, 'French', 'fr');

INSERT INTO public.pronouns (id, language_id, pronoun, person, "number", gender) VALUES
    (19, 3, 'je', 1, 1, -1),
    (20, 3, 'tu', 2, 1, -1),
    (21, 3, 'il', 3, 1, 1),
    (22, 3, 'elle', 3, 1, 2),
    (23, 3, 'nous', 1, 2, -1),
    (24, 3, 'vous', 2, 2, -1),
    (25, 3, 'ils', 3, 2, 1),
    (26, 3, 'elles', 3, 2, 2);

INSERT INTO public.verb_conjugations (id, language_id, conjugation, endings) VALUES
    (6, 3, 1, '{"il": "-e", "je": "-e", "tu": "-es", "ils": "-ent", "nous": "-ons", "vous": "-ez"}'::jsonb),
    (7, 3, 2, '{"il": "-it", "je": "-is", "tu": "-is", "ils": "-issent", "nous": "-issons", "vous": "-issez"}'::jsonb),
    (8, 3, 3, '{"il": "-", "je": "-s", "tu": "-s", "ils": "-ent", "nous": "-ons", "vous": "-ez"}'::jsonb);


INSERT INTO public.verb_regexes (id,language_id,"sequence",property,pattern,replacement) VALUES
	 (2,2,1,'stem','ować$','uj'),
	 (3,2,2,'stem','ć$',''),
	 (1,1,1,'infinitive','^(.*)$','to \1'),
	 (4,3,1,'stem','(er|ir|re)$','');


INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES
    (19, 3, 'parler', '{}'::jsonb, 1),
    (20, 3, 'perdre', '{}'::jsonb, 3),
    (21, 3, 'choisir', '{}'::jsonb, 2),
    (22, 1, 'choose', '{}'::jsonb, 0),
    (23, 1, 'lose', '{}'::jsonb, 0);

INSERT INTO public.verb_translations (id, lang1, lang2) VALUES
    (10, 19, 4),
    (11, 21, 22),
    (12, 20, 23);

---
SELECT pg_catalog.setval('public.languages_id_seq', 3, true);
SELECT pg_catalog.setval('public.pronouns_id_seq', 26, true);
SELECT pg_catalog.setval('public.verb_conjugations_id_seq', 8, true);
SELECT pg_catalog.setval('public.verb_regexes_id_seq', 4, true);
SELECT pg_catalog.setval('public.verb_translations_id_seq', 12, true);
SELECT pg_catalog.setval('public.verbs_id_seq', 23, true);

ALTER TABLE ONLY public.verb_regexes
    ADD CONSTRAINT verb_regexes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.verb_regexes
    ADD CONSTRAINT verb_regexes_language_id_fkey FOREIGN KEY (language_id) REFERENCES public.languages(id);
