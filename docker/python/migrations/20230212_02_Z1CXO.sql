-- 
-- depends: 20230212_01_wYkkv

INSERT INTO public.languages (id,"name",code) VALUES
	 (1,'English','en'),
	 (2,'Polish','pl');

INSERT INTO public.pronouns (id,language_id,pronoun,person,"number",gender) VALUES
	 (1,1,'I',1,1,-1),
	 (2,1,'we',1,2,-1),
	 (3,1,'you',2,1,-1),
	 (4,1,'you',2,2,-1),
	 (5,1,'he',3,1,1),
	 (6,1,'she',3,1,2),
	 (7,1,'it',3,1,3),
	 (8,1,'they',3,2,-1),
	 (9,2,'ja',1,1,-1),
	 (10,2,'my',1,2,-1);
INSERT INTO public.pronouns (id,language_id,pronoun,person,"number",gender) VALUES
	 (11,2,'ty',2,1,-1),
	 (12,2,'wy',2,2,-1),
	 (13,2,'on',3,1,1),
	 (14,2,'ona',3,1,2),
	 (15,2,'ono',3,1,3),
	 (16,2,'oni',3,2,1),
	 (17,2,'one',3,2,2),
	 (18,2,'oni',3,2,3);

INSERT INTO public.verb_conjugations (id,language_id,conjugation,endings) VALUES
	 (1,1,0,'{"I": "-", "he": "-s", "we": "-", "you": "-", "they": "-"}'),
	 (2,2,0,'{"ja": "-", "my": "-", "on": "-", "ty": "-", "wy": "-", "oni": "-"}'),
	 (3,2,1,'{"ja": "-ę", "my": "-emy", "on": "-e", "ty": "-esz", "wy": "-ecie", "oni": "-ą"}'),
	 (4,2,2,'{"ja": "-ę", "my": "-my", "on": "-", "ty": "-sz", "wy": "-cie", "oni": "-ą"}'),
	 (5,2,3,'{"ja": "-m", "my": "-my", "on": "-", "ty": "-sz", "wy": "-cie", "oni": "-ją"}');

INSERT INTO public.verbs (id,language_id,infinitive,irregularities,conjugation) VALUES
	 (1,2,'robić','{}',2),
	 (4,1,'speak','{}',0),
	 (2,1,'do','{"he": "-es"}',0),
	 (3,2,'mówić','{}',2),
	 (5,2,'znać','{}',3),
	 (6,1,'know','{}',0),
	 (7,2,'mieć','{"stem": "ma"}',3),
	 (8,1,'have','{"he": "has"}',0),
	 (9,2,'pisać','{"stem": "pisz"}',1),
	 (10,1,'write','{}',0);
INSERT INTO public.verbs (id,language_id,infinitive,irregularities,conjugation) VALUES
	 (11,2,'grać','{}',3),
	 (12,1,'play','{}',0),
	 (13,2,'czytać','{}',3),
	 (14,1,'read','{}',0),
	 (15,2,'pracować','{}',1),
	 (16,1,'work','{}',0),
	 (17,2,'być','{"ja": "-em", "my": "-eśmy", "ty": "-eś", "wy": "-eście", "oni": "są", "stem": "jest"}',0),
	 (18,1,'be','{"I": "am", "he": "is", "stem": "are"}',0);

INSERT INTO public.verb_translations (id,lang1,lang2) VALUES
	 (1,1,2),
	 (2,4,3),
	 (3,5,6),
	 (4,7,8),
	 (5,9,10),
	 (6,11,12),
	 (7,13,14),
	 (8,15,16),
	 (9,17,18);

--
-- Name: languages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.languages_id_seq', 2, true);


--
-- Name: pronouns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pronouns_id_seq', 18, true);


--
-- Name: verb_conjugations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.verb_conjugations_id_seq', 5, true);


--
-- Name: verb_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.verb_translations_id_seq', 9, true);


--
-- Name: verbs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.verbs_id_seq', 18, true);

