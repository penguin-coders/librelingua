-- 
-- depends: 20240128_01_xZAtZ

INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES(25, 2, 'lubić', '{}'::jsonb, 2);
INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES(26, 2, 'kochać', '{}'::jsonb, 3);
INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES(27, 2, 'jeść', '{"16": "-dzą", "stem": "je"}'::jsonb, 3);
INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES(28, 1, 'like', '{}'::jsonb, 0);
INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES(29, 1, 'love', '{}'::jsonb, 0);
INSERT INTO public.verbs (id, language_id, infinitive, irregularities, conjugation) VALUES(30, 1, 'eat', '{}'::jsonb, 0);

INSERT INTO public.verb_translations (id, lang1, lang2) VALUES(13, 25, 28);
INSERT INTO public.verb_translations (id, lang1, lang2) VALUES(14, 26, 29);
INSERT INTO public.verb_translations (id, lang1, lang2) VALUES(15, 27, 30);
