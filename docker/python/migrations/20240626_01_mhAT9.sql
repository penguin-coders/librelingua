-- 
-- depends: 20240624_03_duZco

INSERT INTO public.words (id, part_of_speech, word_ids, lang, fixed_properties) VALUES(13, 8, '[]'::jsonb, 2, '{}'::jsonb);
INSERT INTO public.words (id, part_of_speech, word_ids, lang, fixed_properties) VALUES(14, 8, '[]'::jsonb, 1, '{}'::jsonb);

INSERT INTO public.contexts (id, ntv_lang_id, tgt_lang_id, context_name) VALUES(5, 1, 2, 'Apples and bananas');

INSERT INTO public.phrases (id, tgt_words, ntv_words, translations) VALUES(7, '[13]'::jsonb, '[14]'::jsonb, '[[0, 0]]'::jsonb);

INSERT INTO public.sentences (id, ntv_word_order, tgt_word_order, context_id, phrases) VALUES(7, '[[0, 0]]'::jsonb, '[[0, 0]]'::jsonb, 5, '[7]'::json);

UPDATE public.words SET part_of_speech=8, word_ids='[]'::jsonb, lang=1, fixed_properties='{"case":"nom","number": 2}'::jsonb WHERE id=12;

--
-- Name: noun_cases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.noun_cases (
    id integer NOT NULL,
    noun_case character varying NOT NULL
);


ALTER TABLE public.noun_cases OWNER TO postgres;

--
-- Name: noun_cases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.noun_cases_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.noun_cases_id_seq OWNER TO postgres;

--
-- Name: noun_cases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.noun_cases_id_seq OWNED BY public.noun_cases.id;

--
-- Name: noun_cases id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_cases ALTER COLUMN id SET DEFAULT nextval('public.noun_cases_id_seq'::regclass);

--
-- Name: noun_cases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.noun_cases_id_seq', 1, false);


--
-- Name: noun_cases noun_cases_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noun_cases
    ADD CONSTRAINT noun_cases_pk PRIMARY KEY (id);

INSERT INTO public.noun_cases (id, noun_case) VALUES(1, 'nom');
INSERT INTO public.noun_cases (id, noun_case) VALUES(2, 'acc');
INSERT INTO public.noun_cases (id, noun_case) VALUES(3, 'gen');
INSERT INTO public.noun_cases (id, noun_case) VALUES(4, 'dat');
INSERT INTO public.noun_cases (id, noun_case) VALUES(5, 'loc');
INSERT INTO public.noun_cases (id, noun_case) VALUES(6, 'ins');
INSERT INTO public.noun_cases (id, noun_case) VALUES(7, 'voc');

CREATE TABLE public.language_cases (
	id serial4 NOT NULL,
	language_id int4 NOT NULL,
	cases jsonb NOT NULL DEFAULT '[]'::jsonb,
	CONSTRAINT language_cases_pk PRIMARY KEY (id)
);

INSERT INTO public.language_cases (id, language_id, cases) VALUES(1, 1, '[1, 3]'::jsonb);
INSERT INTO public.language_cases (id, language_id, cases) VALUES(2, 2, '[1, 2, 3, 4, 5, 6, 7]'::jsonb);
