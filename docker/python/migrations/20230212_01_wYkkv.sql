-- 
-- depends: 

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: languages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.languages (
    id integer NOT NULL,
    name character varying,
    code character(2)
);


ALTER TABLE public.languages OWNER TO postgres;

--
-- Name: languages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.languages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.languages_id_seq OWNER TO postgres;

--
-- Name: languages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.languages_id_seq OWNED BY public.languages.id;


--
-- Name: pronouns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pronouns (
    id integer NOT NULL,
    language_id integer NOT NULL,
    pronoun character varying(10) NOT NULL,
    person smallint NOT NULL,
    number smallint NOT NULL,
    gender smallint NOT NULL
);


ALTER TABLE public.pronouns OWNER TO postgres;

--
-- Name: pronouns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pronouns_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pronouns_id_seq OWNER TO postgres;

--
-- Name: pronouns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pronouns_id_seq OWNED BY public.pronouns.id;


--
-- Name: verb_conjugations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.verb_conjugations (
    id integer NOT NULL,
    language_id integer NOT NULL,
    conjugation smallint NOT NULL,
    endings jsonb NOT NULL
);


ALTER TABLE public.verb_conjugations OWNER TO postgres;

--
-- Name: verb_conjugations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.verb_conjugations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.verb_conjugations_id_seq OWNER TO postgres;

--
-- Name: verb_conjugations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.verb_conjugations_id_seq OWNED BY public.verb_conjugations.id;


--
-- Name: verb_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.verb_translations (
    id integer NOT NULL,
    lang1 integer NOT NULL,
    lang2 integer NOT NULL
);


ALTER TABLE public.verb_translations OWNER TO postgres;

--
-- Name: verb_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.verb_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.verb_translations_id_seq OWNER TO postgres;

--
-- Name: verb_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.verb_translations_id_seq OWNED BY public.verb_translations.id;


--
-- Name: verbs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.verbs (
    id integer NOT NULL,
    language_id integer NOT NULL,
    infinitive character varying(50) NOT NULL,
    irregularities jsonb DEFAULT '{}'::jsonb NOT NULL,
    conjugation integer NOT NULL
);


ALTER TABLE public.verbs OWNER TO postgres;

--
-- Name: verbs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.verbs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.verbs_id_seq OWNER TO postgres;

--
-- Name: verbs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.verbs_id_seq OWNED BY public.verbs.id;


--
-- Name: vw_bidirectional_verb_trans; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vw_bidirectional_verb_trans AS
 SELECT verb_translations.lang1 AS lng1,
    verb_translations.lang2 AS lng2
   FROM public.verb_translations
UNION
 SELECT verb_translations.lang2 AS lng1,
    verb_translations.lang1 AS lng2
   FROM public.verb_translations;


ALTER TABLE public.vw_bidirectional_verb_trans OWNER TO postgres;

--
-- Name: vw_vt_verbs; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vw_vt_verbs AS
 SELECT l1.code AS v1_lang,
    v1.infinitive AS v1_infinitive,
    v1.conjugation AS v1_conjugation,
    v1.irregularities AS v1_irregularities,
    l2.code AS v2_lang,
    v2.infinitive AS v2_infinitive,
    v2.conjugation AS v2_conjugation,
    v2.irregularities AS v2_irregularities
   FROM ((((public.verbs v1
     JOIN public.vw_bidirectional_verb_trans vt ON ((v1.id = vt.lng1)))
     JOIN public.verbs v2 ON ((v2.id = vt.lng2)))
     JOIN public.languages l1 ON ((l1.id = v1.language_id)))
     JOIN public.languages l2 ON ((l2.id = v2.language_id)));


ALTER TABLE public.vw_vt_verbs OWNER TO postgres;

--
-- Name: languages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.languages ALTER COLUMN id SET DEFAULT nextval('public.languages_id_seq'::regclass);


--
-- Name: pronouns id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pronouns ALTER COLUMN id SET DEFAULT nextval('public.pronouns_id_seq'::regclass);


--
-- Name: verb_conjugations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_conjugations ALTER COLUMN id SET DEFAULT nextval('public.verb_conjugations_id_seq'::regclass);


--
-- Name: verb_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_translations ALTER COLUMN id SET DEFAULT nextval('public.verb_translations_id_seq'::regclass);


--
-- Name: verbs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verbs ALTER COLUMN id SET DEFAULT nextval('public.verbs_id_seq'::regclass);

--
-- Name: languages languages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: pronouns pronouns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pronouns
    ADD CONSTRAINT pronouns_pkey PRIMARY KEY (id);


--
-- Name: verb_conjugations verb_conjugations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_conjugations
    ADD CONSTRAINT verb_conjugations_pkey PRIMARY KEY (id);


--
-- Name: verb_translations verb_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_translations
    ADD CONSTRAINT verb_translations_pkey PRIMARY KEY (id);


--
-- Name: verbs verbs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verbs
    ADD CONSTRAINT verbs_pkey PRIMARY KEY (id);


--
-- Name: pronouns pronouns_language_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pronouns
    ADD CONSTRAINT pronouns_language_id_fkey FOREIGN KEY (language_id) REFERENCES public.languages(id);


--
-- Name: verb_conjugations verb_conjugations_language_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_conjugations
    ADD CONSTRAINT verb_conjugations_language_id_fkey FOREIGN KEY (language_id) REFERENCES public.languages(id);


--
-- Name: verb_translations verb_translations_lang1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_translations
    ADD CONSTRAINT verb_translations_lang1_fkey FOREIGN KEY (lang1) REFERENCES public.verbs(id);


--
-- Name: verb_translations verb_translations_lang2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verb_translations
    ADD CONSTRAINT verb_translations_lang2_fkey FOREIGN KEY (lang2) REFERENCES public.verbs(id);


--
-- Name: verbs verbs_language_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verbs
    ADD CONSTRAINT verbs_language_id_fkey FOREIGN KEY (language_id) REFERENCES public.languages(id);


--
-- PostgreSQL database dump complete
--

