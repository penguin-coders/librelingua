-- 
-- depends: 20240128_02_lG4Ef

CREATE TABLE public.contexts (
    id integer NOT NULL,
    ntv_lang_id integer NOT NULL,
    tgt_lang_id integer NOT NULL,
    context_name character varying(50) NOT NULL
);

ALTER TABLE public.contexts OWNER TO postgres;

CREATE SEQUENCE public.contexts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.contexts_id_seq OWNER TO postgres;
ALTER SEQUENCE public.contexts_id_seq OWNED BY public.contexts.id;

CREATE TABLE public.parts_of_speech (
    id integer NOT NULL,
    pos character varying(5) NOT NULL,
    description character varying(30),
    wordclass_type character varying(6) NOT NULL,
    table_name character varying(40)
);

ALTER TABLE public.parts_of_speech OWNER TO postgres;

CREATE SEQUENCE public.parts_of_speech_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.parts_of_speech_id_seq OWNER TO postgres;
ALTER SEQUENCE public.parts_of_speech_id_seq OWNED BY public.parts_of_speech.id;

CREATE SEQUENCE public.phrases_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.phrases_id_seq OWNER TO postgres;

CREATE TABLE public.phrases (
    id integer DEFAULT nextval('public.phrases_id_seq'::regclass) NOT NULL,
    tgt_words jsonb NOT NULL,
    ntv_words jsonb NOT NULL,
    translations jsonb NOT NULL
);

ALTER TABLE public.phrases OWNER TO postgres;

CREATE TABLE public.sentences (
    id integer NOT NULL,
    ntv_word_order jsonb NOT NULL,
    tgt_word_order jsonb NOT NULL,
    context_id integer NOT NULL,
    phrases json
);

ALTER TABLE public.sentences OWNER TO postgres;

CREATE SEQUENCE public.sentences_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.sentences_id_seq OWNER TO postgres;

ALTER SEQUENCE public.sentences_id_seq OWNED BY public.sentences.id;

CREATE SEQUENCE public.words_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.words_id_seq OWNER TO postgres;

CREATE TABLE public.words (
    id integer DEFAULT nextval('public.words_id_seq'::regclass) NOT NULL,
    part_of_speech integer NOT NULL,
    word_ids jsonb DEFAULT '[]'::jsonb NOT NULL,
    lang integer NOT NULL
);

ALTER TABLE public.words OWNER TO postgres;

ALTER TABLE ONLY public.contexts ALTER COLUMN id SET DEFAULT nextval('public.contexts_id_seq'::regclass);

ALTER TABLE ONLY public.parts_of_speech ALTER COLUMN id SET DEFAULT nextval('public.parts_of_speech_id_seq'::regclass);

ALTER TABLE ONLY public.sentences ALTER COLUMN id SET DEFAULT nextval('public.sentences_id_seq'::regclass);

INSERT INTO public.contexts (ntv_lang_id,tgt_lang_id,context_name) VALUES
	 (1,2,'Polish verbs - present tense'),
	 (1,3,'French verbs - present tense');
	
INSERT INTO public.parts_of_speech (pos,description,wordclass_type,table_name) VALUES
	 ('ADJ','adjective','open',''),
	 ('ADP','adposition','closed',''),
	 ('ADV','adverb','open',''),
	 ('AUX','auxiliary','closed',''),
	 ('CCONJ','coordinating conjunction','closed',''),
	 ('DET','determiner','closed',''),
	 ('INTJ','interjection','open',''),
	 ('NOUN','noun','open',''),
	 ('NUM','numeral','closed',''),
	 ('PART','particle','closed','');

INSERT INTO public.parts_of_speech (pos,description,wordclass_type,table_name) VALUES
	 ('PRON','pronoun','closed','pronouns'),
	 ('PROPN','proper noun','open',''),
	 ('PUNCT','punctuation','other',''),
	 ('SCONJ','subordinating conjunction','closed',''),
	 ('SYM','symbol','other',''),
	 ('VERB','verb','open','verbs'),
	 ('X','other','other','');

INSERT INTO public.phrases (tgt_words,ntv_words,translations) VALUES
	 ('[2]','[4, 1]','[[1, 0]]'),
	 ('[5, 3]','[4, 1]','[[0, 0], [1, 1]]');

INSERT INTO public.sentences (ntv_word_order,tgt_word_order,context_id,phrases) VALUES
	 ('[[0, 0], [0, 1]]','[[0, 0]]',1,'[1]'),
	 ('[[0, 0], [0, 1]]','[[0, 0], [0, 1]]',2,'[2]');
	
INSERT INTO public.words (part_of_speech,word_ids,lang) VALUES
	 (16,'[]',3),
	 (16,'[]',1),
	 (16,'[]',2),
	 (11,'[]',1),
	 (11,'[]',3);

SELECT pg_catalog.setval('public.contexts_id_seq', 2, true);

SELECT pg_catalog.setval('public.parts_of_speech_id_seq', 17, false);

SELECT pg_catalog.setval('public.phrases_id_seq', 2, true);

SELECT pg_catalog.setval('public.sentences_id_seq', 2, true);

SELECT pg_catalog.setval('public.words_id_seq', 5, true);

ALTER TABLE ONLY public.contexts
    ADD CONSTRAINT contexts_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.parts_of_speech
    ADD CONSTRAINT parts_of_speech_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.sentences
    ADD CONSTRAINT sentences_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.contexts
    ADD CONSTRAINT contexts_ntv_lang_id_fkey FOREIGN KEY (ntv_lang_id) REFERENCES public.languages(id);

ALTER TABLE ONLY public.contexts
    ADD CONSTRAINT contexts_tgt_lang_id_fkey FOREIGN KEY (tgt_lang_id) REFERENCES public.languages(id);

ALTER TABLE ONLY public.sentences
    ADD CONSTRAINT sentences_context_id_fkey FOREIGN KEY (context_id) REFERENCES public.contexts(id);

ALTER TABLE ONLY public.words
    ADD CONSTRAINT words_fk FOREIGN KEY (part_of_speech) REFERENCES public.parts_of_speech(id);

ALTER TABLE ONLY public.words
    ADD CONSTRAINT words_lang_fk FOREIGN KEY (lang) REFERENCES public.languages(id);
