-- 
-- depends: 20240626_01_mhAT9

ALTER TABLE public.sentences ADD disambiguations jsonb NOT NULL DEFAULT '[]';
UPDATE public.sentences SET disambiguations='[["tgt", 0, 0, "case"]]'::jsonb WHERE id=7;
