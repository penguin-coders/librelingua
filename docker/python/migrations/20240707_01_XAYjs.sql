-- 
-- depends: 20240627_01_gIDWo

ALTER VIEW vw_nt_nouns  RENAME COLUMN n1_id to lang1_id;
ALTER VIEW vw_nt_nouns  RENAME COLUMN n2_id to lang2_id;
ALTER VIEW vw_vt_verbs  RENAME COLUMN v1_id to lang1_id;
ALTER VIEW vw_vt_verbs  RENAME COLUMN v2_id to lang2_id;

SELECT pg_catalog.setval('sentences_id_seq', 7, true);
SELECT pg_catalog.setval('phrases_id_seq', 7, true);
SELECT pg_catalog.setval('words_id_seq', 14, true);

UPDATE parts_of_speech SET table_name='nouns' WHERE id=8;
