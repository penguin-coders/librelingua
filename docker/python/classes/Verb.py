##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import re
import database
from classes.Pronouns import *

class Verb():

    def __init__(self, lang, infinitive, conjugation, endings, third_person_pronouns, regexes , irregularities = {}, fixed_props = {}):
        
        # Set properties passed as params
        self.language = lang
        self.infinitive = infinitive
        self.conjugation = conjugation

        # Set pronouns for language
        self.pronouns = Pronouns(lang)
        
        # Determine verb stem
        self.stem = infinitive
        self.regexes = regexes
        self._do_regexes()

        # Set verb endings, substituting in any irregularities
        self.endings = endings
        self._set_props({**self.endings[self.conjugation],**irregularities})
        
        # Copy endings for fem and neut pronouns from masc
        self.third_person_pronouns = third_person_pronouns
        for masc in [prn for prn in self.third_person_pronouns if prn['gender']==1]:
            for non_masc in [prn['id'] for prn in self.third_person_pronouns if prn['gender']!=1 and prn['number']==masc['number']]:
                self.__dict__[f"{non_masc}"] = self.__dict__[f"{masc['id']}"]

        # Set any fixed properties, or set sane defaults
        self.verb_form = 'indicative' 
        self._set_props(fixed_props)

    def __str__(self):
        return(f"{self.infinitive}")
                
    # Set object properties specified in prop_dict
    def _set_props(self, prop_dict):
        for prop, val in prop_dict.items():
            self.__dict__[prop] = val

    def _do_regexes(self):
        for reg in self.regexes: 
            self.__dict__[reg['property']] = re.sub(reg['pattern'], reg['replacement'], self.__dict__[reg['property']])

    def conjugate(self, pronoun):
        return re.sub("^-", self.stem, self.__dict__[f"{pronoun}"])

    def get_word(self, subject):
        if(self.verb_form=='infinitive'):
            return self.infinitive
        return self.conjugate(self.pronouns.get_pronoun_id(subject))

########## End of class definition ##############

def get_endings_from_db(lang):

    endings = {}
    
    query = """
            SELECT vc.* FROM verb_conjugations AS vc
                JOIN languages AS l ON l.id = vc.language_id
                WHERE l.code = %s
            """

    for vb in database.db.fetchall(query, (lang,)):
        endings[vb['conjugation']] = vb['endings']

    return endings

def get_third_person_pronouns_from_db(lang):

    query = """
            SELECT p.* FROM pronouns p 
                JOIN languages l ON l.id = p.language_id 
                WHERE l.code = %s AND person = 3
            """
    return database.db.fetchall(query, (lang,))

def get_regexes_from_db(lang):

    query = """
        SELECT r.* FROM verb_regexes r 
            JOIN languages l ON l.id = r.language_id 
            WHERE l.code = %s 
            ORDER BY sequence
        """

    return database.db.fetchall(query, (lang,))
