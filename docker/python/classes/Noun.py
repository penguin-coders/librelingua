import re
import database
import random

class Noun():

    def __init__(self, lang, lemma, inflection, endings, lang_cases, all_cases,
                 irregularities = {}, fixed_props = {}):
        self.lang = lang
        self.lemma = lemma
        self.inflection = inflection
        self.endings = endings[inflection]
        for num, cases in irregularities.items():
            for case, ending in cases.items():
                self.endings[num][case] = ending
        pat = re.sub("^-", "", self.endings['1']['nom'])
        self.stem = re.sub(pat, "", self.lemma)

        # Set any fixed properties, or set sane defaults
        self.case = "nom"
        self.allcases = all_cases
        self.cases = lang_cases
        self.number = 1
        self._set_props(fixed_props)
    
    # Set object properties specified in prop_dict
    def _set_props(self, prop_dict):
        for prop, val in prop_dict.items():
            self.__dict__[prop] = val
            self.__dict__[prop+"s"] = [val]

    def decline(self):
        return re.sub("^-", self.stem, self.endings[str(self.number)][self.case])

    def get_word(self, subject):
       return self.decline()

    def get_disambiguation(self, prop):
        if(prop=="case"):
            return self.case
        return ""

    def randomise(self, **params):
        if("case" in params):
            selected_case = params["case"]
        else:
            selected_case = self.allcases[random.randrange(0, 7)]
        self.case = selected_case
        if(not selected_case in self.cases):
            if(len(self.cases)==1):
                self.case = self.cases[0]
            elif("nom" in self.cases):
                self.case = "nom"
            else:
                pass
                # flag an error
        return { "case": selected_case }

########## End of class definition ##############

def get_declensions_from_db(lang):

    endings = {}
    
    query = """
            SELECT nd.* FROM noun_declensions AS nd
                JOIN languages AS l ON l.id = nd.language_id
                WHERE l.code = %s
            """

    for res in database.db.fetchall(query, (lang,)):
        endings[res['declension']] = res['endings']

    return endings

def get_cases_from_db(lang):
   
    query = """
            SELECT nc.noun_case AS case FROM
            (SELECT jsonb_array_elements(cases) AS case_id 
                FROM language_cases lc
                JOIN languages l ON l.id=lc.language_id
                WHERE l.code=%s) c
            JOIN noun_cases nc 
            ON nc.id=c.case_id::integer;
            """
    
    results =  database.db.fetchall(query, (lang,))

    return [r['case'] for r in results]
