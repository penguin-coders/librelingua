##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from typing import Optional

from classes.Subject import *
import database

class Pronouns():

    def __init__(self, lang):
        self.language = lang
        self.pronouns = get_pronouns_from_db(self.language)
        self.ambiguous_number = get_ambiguous_pronouns_from_db(self.language)
        self.subject = Subject()
    
    def __str__(self):
        return self.get_pronoun()

    def get_word(self, subject: Optional[Subject] = None):
        return f"{self.get_pronoun(subject)}{self.get_disambiguation()}"

    def get_pronoun(self, subject: Optional[Subject] = None):
        if(subject != None):
            self.subject = subject
        return self.pronouns[self.subject.gender][self.subject.person][self.subject.number][1]

    def get_pronoun_id(self, subject: Optional[Subject] = None):
        if(subject != None):
            self.subject = subject
        return self.pronouns[self.subject.gender][self.subject.person][self.subject.number][0]
    
    def get_disambiguation(self, subject: Optional[Subject] = None):
        if(subject != None):
            self.subject = subject
        if(self.get_pronoun() in self.ambiguous_number):
            return '(sing)' if self.subject.number==1 else '(pl)'
        return ''

def get_pronouns_from_db(lang):
    
    query = """
            SELECT p.* FROM vw_pronouns AS p
                JOIN languages AS l ON l.id = p.language_id
                WHERE l.code = %s
            """
    results = database.db.fetchall(query,(lang,))

    pronouns = {}
    for i in range(1,4):
        pronouns[i] = {}
        for j in range(1,4):
            pronouns[i][j] = {}

    for r in results:
        pronouns[r['gender']][r['person']][r['number']] = (r['id'],r['pronoun'])

    return pronouns

def get_ambiguous_pronouns_from_db(lang):

    query = """
        SELECT p.pronoun FROM
            (SELECT pr.pronoun, COUNT(pr.number) AS pronoun_count FROM pronouns pr 
            JOIN languages l ON l.id = pr.language_id 
            WHERE l.code = %s GROUP BY pr.pronoun) p
        WHERE p.pronoun_count > 1;
        """
    results = database.db.fetchall(query, (lang,))

    return [r['pronoun'] for r in results]
