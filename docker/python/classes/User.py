##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from passlib import hash
import jwt as jwt
from fastapi import HTTPException, Depends
import fastapi.security as sec 

import database
from config import get_jwt_secret

jwt_secret = get_jwt_secret()
oauth2schema = sec.OAuth2PasswordBearer(tokenUrl="/user/login")

class User:
	
    def __init__(self, email, passwd, lang = 0):
        self.id = -1
        self.email = email
        self.passwd = passwd
        self.tgt_lang = lang
        self.ntv_lang = 1

    def register(self):
        hashed_passwd = hash.bcrypt.hash(self.passwd)
        self.id = database.db.insert("INSERT INTO users (email, passwd) VALUES (%s, %s) RETURNING id", (self.email, hashed_passwd))
        database.db.insert("INSERT INTO user_languages (user_id, ntv_lang_id, tgt_lang_id) VALUES (%s, %s, %s)", (self.id, self.ntv_lang, self.tgt_lang))

        # temp hack
        database.db.insert("INSERT INTO public.user_contexts (user_id,tgt_lang_id,context_id) VALUES (%s,2,1), (%s,3,2)", (self.id, self.id))
    
    def authenticate(self):
        db_user = database.db.fetchall("SELECT id, passwd FROM users WHERE email = %s",(self.email,))[0]
        db_passwd = db_user['passwd']
        if not hash.bcrypt.verify(self.passwd, db_passwd):
            return False
        self.id = db_user['id']
        return True

    def create_token(self):
        token = jwt.encode({"id":self.id,"email":self.email}, jwt_secret)
        return dict(access_token=token, token_type="bearer")

def get_user_id_from_token(token: str = Depends(oauth2schema)):
    try:
        payload = jwt.decode(token, jwt_secret, algorithms=["HS256"])
        user_id = database.db.fetchall("SELECT id FROM users WHERE id = %s",(payload["id"],))[0]['id']
    except:
        raise HTTPException(
            status_code=401, detail="Invalid Email or Password"
        )

    return user_id 

