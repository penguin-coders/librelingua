##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import random
from random import choice
from classes.Subject import Person, Number, Gender, Subject
from classes.Pronouns import *
from classes.Verb import *

class Sentence():
    def __init__(self, subject, phrases, ntv_word_order, tgt_word_order, disambiguations = {}):
        self.subject = subject
        self.phrases = phrases
        self.ntv_word_order = ntv_word_order
        self.tgt_word_order = tgt_word_order
        self.disambiguations = disambiguations
        
    def set_subject(self, subject):
        self.subject = subject

    def randomise(self):
        for p in self.phrases:
            p.rand_selection()
            p.generate_words(self.subject)
    
    def get_ntv_sentence(self):
        self.ntv_sentence = []
        for (p,w) in self.ntv_word_order:
            temp = self.phrases[p].ntv_generated_words[w]
            self.ntv_sentence.append(temp)
        return " ".join(self.ntv_sentence)

    def get_tgt_sentence(self):
        self.tgt_sentence = []
        for (p,w) in self.tgt_word_order:
            temp = self.phrases[p].tgt_generated_words[w]
            self.tgt_sentence.append(temp)
        return " ".join(self.tgt_sentence)
   
    def get_disambiguation(self):
        disambiguations = []
        for dis in self.disambiguations:
            if(dis[0]=="tgt"):
                disambiguations.append(
                        self.phrases[dis[1]].tgt_word_objs[dis[2]].get_disambiguation(dis[3]))
        returnStr = ", ".join(disambiguations)
        return "" if(returnStr=="") else f"({returnStr})"
