##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import random

class Word():
    def __init__(self, pos, options):
        self.options = options
        self.pos = pos
        self.selected_option = 0
        self.num_options = len(options)
    def get_word(self, subject):
        return self.options[self.selected_option].get_word(subject)
    def select_option(self, selected_option='', **word_params):
        if(selected_option!=''):
            self.selected_option = selected_option
        else:
            self.selected_option = random.randrange(0, self.num_options)
        if(self.pos=="NOUN"):
            word_params = self.options[self.selected_option].randomise(**word_params)
        return { "selected_option" : self.selected_option, **word_params }
    def get_disambiguation(self, param):
        if(self.pos=="NOUN"):
            return self.options[self.selected_option].get_disambiguation(param)
        return ""
        
