##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

#import random
from classes.Pronouns import *
from classes.Verb import *

class Phrase():
    def __init__(self, ntv_words, tgt_words, translations):
        self.ntv_word_objs = ntv_words
        self.tgt_word_objs = tgt_words
        self.translations = translations
        self.ntv_generated_words = []
        self.tgt_generated_words = []
    def rand_selection(self):
        trans_ntv, trans_tgt = zip(*self.translations)
        untrans_ntv = [i for i in self.ntv_word_objs if i not in trans_ntv]
        untrans_tgt = [i for i in self.tgt_word_objs if i not in trans_tgt]
        for ntv_w in untrans_ntv:
            ntv_w.select_option()
        for tgt_w in untrans_tgt:
            tgt_w.select_option()
        for ntv_idx, tgt_idx in self.translations:
            ntv_w = self.ntv_word_objs[ntv_idx]
            tgt_w = self.tgt_word_objs[tgt_idx]
            tgt_w.select_option(**ntv_w.select_option())
    def generate_words(self, subject):
        self.ntv_generated_words = []
        for w in self.ntv_word_objs:
            self.ntv_generated_words.append(w.get_word(subject))
        self.tgt_generated_words = []
        for w in self.tgt_word_objs:
            self.tgt_generated_words.append(w.get_word(subject))
