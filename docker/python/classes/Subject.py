##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

from enum import Enum
from pydantic import BaseModel

class Person(Enum):
    FIRST = 1
    SECOND = 2
    THIRD = 3

class Number(Enum):
    SINGULAR = 1 
    PLURAL = 2

class Gender(Enum):
    MASC = 1
    FEM = 2
    NEUT = 3

class Subject(BaseModel):
    person: Person = Person.FIRST.value
    number: Number = Number.SINGULAR.value
    gender: Gender = Gender.NEUT.value

    class Config:
        validate_assignment = True
        use_enum_values = True

