##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

from classes.Verb import Verb
from classes.Subject import *
from classes.Pronouns import *

class VerbTranslator():

    def __init__(self, verb_tgt: Verb, verb_ntv: Verb, subject = Subject()):
        self.verb_tgt = verb_tgt
        self.verb_ntv = verb_ntv
        self.subject = subject
        self.pronouns_tgt = Pronouns(self.verb_tgt.language)
        self.pronouns_ntv = Pronouns(self.verb_ntv.language)


    def __str__(self):
        return(f"{self.verb_tgt.infinitive}/{self.verb_ntv.infinitive}")

    def set_subject(self, subject):
        self.subject = subject

    def get_conjugation_tgt(self):
        return self.verb_tgt.conjugate(self.pronouns_tgt.get_pronoun(self.subject)) 
    
    def get_conjugation_ntv(self):
        return self.verb_ntv.conjugate(self.pronouns_ntv.get_pronoun(self.subject)) 

    def get_pronoun_ntv(self):
        return self.pronouns_ntv.get_pronoun(self.subject)

    def get_pronoun_tgt(self):
        return self.pronouns_tgt.get_pronoun(self.subject)
    
    def get_disambiguation_ntv(self):
        return self.pronouns_ntv.get_disambiguation(self.subject)

