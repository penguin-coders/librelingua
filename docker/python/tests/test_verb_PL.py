##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

import pytest

from classes.Verb import *

@pytest.mark.parametrize("infinitive,conjugation,params,expected_conjugation",
                         [
                             ("robić", 2, {}, "robić: robię, robisz, robi, robimy, robicie, robią"),
                             ("mówić", 2, {}, "mówić: mówię, mówisz, mówi, mówimy, mówicie, mówią"),
                             ("znać", 3, {}, "znać: znam, znasz, zna, znamy, znacie, znają"),
                             ("mieć", 3, {'stem':'ma'}, "mieć: mam, masz, ma, mamy, macie, mają"),
                             ("pisać", 1, {'stem':'pisz'}, "pisać: piszę, piszesz, pisze, piszemy, piszecie, piszą"),
                             ("grać", 3, {}, "grać: gram, grasz, gra, gramy, gracie, grają"),
                             ("czytać", 3, {}, "czytać: czytam, czytasz, czyta, czytamy, czytacie, czytają"),
                             ("pracować", 1, {}, "pracować: pracuję, pracujesz, pracuje, pracujemy, pracujecie, pracują"),
                             ("być", 0, {'stem':'jest','ja':'-em','ty':'-eś','my':'-eśmy','wy':'-eście','oni':'są'}, "być: jestem, jesteś, jest, jesteśmy, jesteście, są"),
                         ])
def test_verb_conjugation_is_correct(infinitive, conjugation, params, expected_conjugation):
    verb = Verb('pl',infinitive, conjugation, params)
    conj = verb.infinitive+": "+", ".join([verb.conjugate(p) for p in ['ja','ty','on','my','wy','oni']])
    assert conj == expected_conjugation

@pytest.mark.parametrize("pronoun",['ja','ty','on','ona','ono','my','wy','oni','one'])
def test_all_pronouns_defined(pronoun):
    verb = Verb('pl','mowić',2)
    assert hasattr(verb, pronoun)
