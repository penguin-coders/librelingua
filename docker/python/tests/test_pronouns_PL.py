##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

import pytest
from unittest.mock import Mock

from classes.Pronouns import Pronouns
from classes.Subject import Subject

GENDER_MASC = 1
GENDER_FEM = 2
GENDER_NEUT = 3

PERSON_FIRST = 1
PERSON_SECOND = 2
PERSON_THIRD = 3

NUMBER_SINGULAR = 1
NUMBER_PLURAL = 2

class TestPronounsPL:
    
    def setup_method(self):
        self.proPL = Pronouns('pl')
        
    @pytest.mark.parametrize("params,expected", 
            [
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"ja"),
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"ja"),
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"ja"),
                
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"my"),
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"my"),
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"my"),
                
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"ty"),
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"ty"),
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"ty"),
                
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"wy"),
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"wy"),
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"wy"),
                
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"on"),
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"ona"),
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"ono"),

                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"oni"),
                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"one"),
                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"oni"),
            ])
    def test_returned_pronouns_are_correct(self, params, expected):
        mock_subject = Mock(Subject)
        mock_subject.person = params['person']
        mock_subject.number = params['number']
        mock_subject.gender = params['gender']
        mock_subject._copy_and_set_values.return_value = mock_subject # See https://github.com/pydantic/pydantic/discussions/4537
        assert self.proPL.get_pronoun(mock_subject) == expected

