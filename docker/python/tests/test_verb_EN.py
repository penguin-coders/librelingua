##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

import pytest

from classes.Verb import *

@pytest.mark.parametrize("infinitive, irregularities, expected_conjugation",
                         [
                             ("do", {'he':"-es"}, "to do: do, do, does, do, do, do"),
                             ("speak", {}, "to speak: speak, speak, speaks, speak, speak, speak"),
                             ("know", {}, "to know: know, know, knows, know, know, know"),
                             ("have", {'he':"has"}, "to have: have, have, has, have, have, have"),
                             ("write", {}, "to write: write, write, writes, write, write, write"),
                             ("play", {}, "to play: play, play, plays, play, play, play"),
                             ("read", {}, "to read: read, read, reads, read, read, read"),
                             ("work", {}, "to work: work, work, works, work, work, work"),
                             ("be", {'stem':"are", 'I':"am", 'he':"is"}, "to be: am, are, is, are, are, are"),
                         ])
def test_verb_conjugation_is_correct(infinitive, irregularities, expected_conjugation):
    verb = Verb('en',infinitive, 0, irregularities)
    conj = verb.infinitive+": "+", ".join([verb.conjugate(p) for p in ['I','you','he','we','you','they']])
    assert conj == expected_conjugation

@pytest.mark.parametrize("pronoun",['I','you','he','she','it','we','they'])
def test_all_pronouns_defined(pronoun):
    verb = Verb('en','read',0)
    assert hasattr(verb, pronoun)
    

