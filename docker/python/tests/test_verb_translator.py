##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

import pytest
from unittest.mock import Mock

from classes.Verb import *
from classes.VerbTranslator import *

GENDER_NEUT = 3

@pytest.mark.parametrize("infPL, conjPL, irregPL, infEN, irregEN, conjugations",
                         [
                             ("robić", 2, {}, "do", {'he':"-es"}, [[1,1,'robię','do'], [2,1,'robisz','do'], [3,1,'robi','does'], 
                                                                  [1,2,'robimy','do'], [2,2,'robicie','do'], [3,2,'robią','do']]),
                             ("mówić", 2, {}, "speak", {}, [[1,1,'mówię','speak'], [2,1,'mówisz','speak'], [3,1,'mówi','speaks'], 
                                                                  [1,2,'mówimy','speak'], [2,2,'mówicie','speak'], [3,2,'mówią','speak']]),
                             ("znać", 3, {}, "know", {}, [[1,1,'znam','know'], [2,1,'znasz','know'], [3,1,'zna','knows'], 
                                                                  [1,2,'znamy','know'], [2,2,'znacie','know'], [3,2,'znają','know']]),
                             ("pisać", 1,{'stem':'pisz'}, "write", {}, [[1,1,'piszę','write'], [2,1,'piszesz','write'], [3,1,'pisze','writes'], 
                                                                  [1,2,'piszemy','write'], [2,2,'piszecie','write'], [3,2,'piszą','write']]),
                             ("grać", 3, {}, "play", {}, [[1,1,'gram','play'], [2,1,'grasz','play'], [3,1,'gra','plays'], 
                                                                  [1,2,'gramy','play'], [2,2,'gracie','play'], [3,2,'grają','play']]),
                             ("czytać", 3, {}, "read", {}, [[1,1,'czytam','read'], [2,1,'czytasz','read'], [3,1,'czyta','reads'], 
                                                                  [1,2,'czytamy','read'], [2,2,'czytacie','read'], [3,2,'czytają','read']]),
                             ("pracować", 1, {}, "work", {}, [[1,1,'pracuję','work'], [2,1,'pracujesz','work'], [3,1,'pracuje','works'], 
                                                                  [1,2,'pracujemy','work'], [2,2,'pracujecie','work'], [3,2,'pracują','work']]),
                             ("być", 0, {'stem':'jest','ja':'-em','ty':'-eś','my':'-eśmy','wy':'-eście','oni':'są'}, "be", {'stem':"are", 'I':"am", 'he':"is"}, 
                                                                  [[1,1,'jestem','am'], [2,1,'jesteś','are'], [3,1,'jest','is'], 
                                                                  [1,2,'jesteśmy','are'], [2,2,'jesteście','are'], [3,2,'są','are']]),
                             ("mieć", 3, {'stem':'ma'}, "have", {'he':"has"}, [[1,1,'mam','have'], [2,1,'masz','have'], [3,1,'ma','has'], 
                                                                  [1,2,'mamy','have'], [2,2,'macie','have'], [3,2,'mają','have']]),
                         ])
def test_conjugation_based_on_subject(infPL, conjPL, irregPL, infEN, irregEN, conjugations):
    verbPL = Verb('pl', infPL, conjPL, irregPL)
    verbEN = Verb('en', infEN, 0, irregEN)
    verb_trans = VerbTranslator(verbPL, verbEN)
    mock_subject = Mock(Subject)
    mock_subject.gender = GENDER_NEUT 
    for conj in conjugations:
        person, number, polish, english = conj
        mock_subject.person = person
        mock_subject.number = number
        mock_subject._copy_and_set_values.return_value = mock_subject # See https://github.com/pydantic/pydantic/discussions/4537
        verb_trans.set_subject(mock_subject)
        assert verb_trans.verb_tgt.conjugate(verb_trans.pronouns_tgt.get_pronoun(verb_trans.subject)) == polish
        assert verb_trans.verb_ntv.conjugate(verb_trans.pronouns_ntv.get_pronoun(verb_trans.subject)) == english
        assert verb_trans.get_conjugation_tgt() == polish
        assert verb_trans.get_conjugation_ntv() == english


