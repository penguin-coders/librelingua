##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

import pytest
from pydantic import ValidationError,BaseModel

from classes.Subject import *

GENDER_MASC = 1
GENDER_FEM = 2
GENDER_NEUT = 3

PERSON_FIRST = 1
PERSON_SECOND = 2
PERSON_THIRD = 3

NUMBER_SINGULAR = 1
NUMBER_PLURAL = 2

class TestSubject:

    def setup_method(self):
        self.subject = Subject()

    @pytest.mark.parametrize("params",
                             [
                                 ({"person":PERSON_THIRD, "number":NUMBER_PLURAL, "gender":GENDER_FEM}),
                                 ({"person":PERSON_SECOND, "number":NUMBER_SINGULAR, "gender":GENDER_MASC}),
                                 ({"person":PERSON_FIRST, "number":NUMBER_PLURAL, "gender":GENDER_NEUT}),
                             ])
    def test_object_creation_with_valid_input(self,params):
            self.subject = Subject(**params)
            for key,val in params.items():
                assert eval(f"self.subject.{key}") == val

    @pytest.mark.parametrize("params",
                             [
                                 ({"person":PERSON_THIRD, "number":NUMBER_PLURAL, "gender":GENDER_FEM}),
                                 ({"person":PERSON_SECOND, "number":NUMBER_SINGULAR, "gender":GENDER_MASC}),
                                 ({"person":PERSON_FIRST, "number":NUMBER_PLURAL, "gender":GENDER_NEUT}),
                             ])
    def test_property_setting_with_valid_input(self,params):
            for key,val in params.items():
                exec(f"self.subject.{key} = {val}")
                assert eval(f"self.subject.{key}") == val
                    
    @pytest.mark.parametrize("params",
                             [
                                 ({"person":4}),
                                 ({"number":3}),
                                 ({"gender":4}),
                             ])
    def test_object_creation_with_invalid_input_raises_exception(self,params):
        with pytest.raises(ValidationError) as einfo:
            subject = Subject(**params)
        assert "value is not a valid enumeration member" in str(einfo.value)


    
    @pytest.mark.parametrize("params,error",
                             [
                                 ({"person":4}, ValidationError),
                                 ({"number":3}, ValidationError),
                                 ({"gender":4}, ValidationError),
                                 ({"randomprop":"randomval"}, NameError),
                             ])
    def test_property_setting_with_invalid_input_raises_exception(self,params,error):
        with pytest.raises(error) as einfo:
            for key,val in params.items():
                exec(f"self.subject.{key} = {val}")
