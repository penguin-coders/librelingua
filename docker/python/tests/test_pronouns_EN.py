##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import sys
sys.path.append("..")

import pytest
from unittest.mock import Mock

from classes.Pronouns import Pronouns
from classes.Subject import Subject

GENDER_MASC = 1
GENDER_FEM = 2
GENDER_NEUT = 3

PERSON_FIRST = 1
PERSON_SECOND = 2
PERSON_THIRD = 3

NUMBER_SINGULAR = 1
NUMBER_PLURAL = 2

class TestPronounsEN:
    
    def setup_method(self):
        self.proEN = Pronouns('en')
        
    @pytest.mark.parametrize("params,expected", 
            [
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"I"),
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"I"),
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"I"),
                
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"we"),
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"we"),
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"we"),
                
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"you"),
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"you"),
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"you"),
                
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"you"),
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"you"),
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"you"),
                
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"he"),
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"she"),
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"it"),

                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"they"),
                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"they"),
                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"they"),
            ])
    def test_returned_pronouns_are_correct(self, params, expected):
        mock_subject = Mock(Subject)
        mock_subject.person = params['person']
        mock_subject.number = params['number']
        mock_subject.gender = params['gender']
        mock_subject._copy_and_set_values.return_value = mock_subject # See https://github.com/pydantic/pydantic/discussions/4537
        assert self.proEN.get_pronoun(mock_subject) == expected

    @pytest.mark.parametrize("params,expected", 
            [
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},""),
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},""),
                ({"person":PERSON_FIRST,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},""),
                
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_MASC},""),
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_FEM},""),
                ({"person":PERSON_FIRST,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},""),

                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},"(sing)"),
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},"(sing)"),
                ({"person":PERSON_SECOND,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},"(sing)"),
                
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_MASC},"(pl)"),
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_FEM},"(pl)"),
                ({"person":PERSON_SECOND,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},"(pl)"),
                
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_MASC},""),
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_FEM},""),
                ({"person":PERSON_THIRD,"number":NUMBER_SINGULAR,"gender":GENDER_NEUT},""),

                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_MASC},""),
                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_FEM},""),
                ({"person":PERSON_THIRD,"number":NUMBER_PLURAL,"gender":GENDER_NEUT},""),
                
            ])
    def test_pronoun_disambiguation(self, params, expected):
        mock_subject = Mock(Subject)
        mock_subject.person = params['person']
        mock_subject.number = params['number']
        mock_subject.gender = params['gender']
        mock_subject._copy_and_set_values.return_value = mock_subject # See https://github.com/pydantic/pydantic/discussions/4537
        assert self.proEN.get_disambiguation(mock_subject) == expected

