##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from configparser import ConfigParser

def get_config(filename, section):
    cfg_parser = ConfigParser()
    cfg_parser.read(filename)
    if not cfg_parser.has_section(section):
        raise Exception(f"Config section {section} not found in file {filename}")
    params = cfg_parser.items(section)
    return { param[0]: param[1] for param in params }

def get_db():
    return get_config("config.ini","database")

def get_jwt_secret():
    return get_config("config.ini","jwt")['jwt_secret']

