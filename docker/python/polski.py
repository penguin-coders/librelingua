##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import os
from time import sleep
from random import choice

from classes.Subject import Person, Number, Gender, Subject
from classes.Verb import VerbPL, VerbEN
from classes.VerbTranslator import VerbTranslator

def clear_screen():
   if os.name == 'posix':
      os.system('clear')
   else:
      # Windows
      os.system('cls')

def main():

    # verbs to test
    verbs = [
                VerbTranslator(VerbPL("robić", 2), VerbEN("do", {'he':'-es'})),
                VerbTranslator(VerbPL("mówić", 2), VerbEN("speak")),
                VerbTranslator(VerbPL("znać", 3), VerbEN("know")),
                VerbTranslator(VerbPL("mieć", 3, {'stem':'ma'}), VerbEN("have", {'he':'has'})),
                VerbTranslator(VerbPL("pisać", 1, {'stem':'pisz'}), VerbEN("write")),
                VerbTranslator(VerbPL("grać", 3), VerbEN("play")),
                VerbTranslator(VerbPL("czytać", 3), VerbEN("read")),
                VerbTranslator(VerbPL("pracować", 1), VerbEN("work")),
                VerbTranslator(VerbPL("być", 0, {'stem':'jest','ja':'-em','ty':'-eś','my':'-eśmy','wy':'-eście','oni':'są'}), 
                               VerbEN("be", {'stem':'are', 'I':'am', 'he':'is'})),
            ]

    # define spacing and prompt
    len_space = 5
    short_space = ' '*len_space
    prompt = f"Type in Polish:{short_space}"
    long_space = ' '*(len_space+len(prompt))

    # loop until 'exit' is typed
    while(True):

        # choose a verb at random
        verb = choice(verbs)

        # randomly choose paramaters for the subject (i.e. the subject of the verb)
        person = choice(list(Person))
        number = choice(list(Number))
        gender = choice(list(Gender))
        verb.set_subject(Subject(person=person, number=number, gender=gender))

        # get strings in both languages
        str_EN = f"{verb.get_pronoun_EN()} {verb.get_conjugation_EN()} {verb.get_disambiguation_EN()}"
        str_PL = verb.get_conjugation_PL() 
    
        # clear screen and prompt user
        clear_screen()
        print("\n"*2)
        print(f"{long_space}{str_EN}\n")
        answer = input(f"{short_space}{prompt}")
        
        # check answer and print response
        if(answer=='exit'):
            break
        elif(answer==str_PL):
            response = "Correct!!"
        else:
            response = "Sorry, that's not correct."
        print(f"\n{long_space}{response}")

        # pause before looping
        sleep(1)

    # just some newlines for spacing
    print("\n"*2)

if __name__=='__main__':
    main()
