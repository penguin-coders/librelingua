##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from fastapi import FastAPI, HTTPException, Response, Depends
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware 

import database
from routers import userRouter, languageRouter, contextRouter, testRouter
import user

from classes.User import *

app = FastAPI(middleware=[Middleware(CORSMiddleware, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"], allow_credentials=True)])

@app.on_event("startup")
def setup_app():
    # Initialise database and run migrations
    database.init_db()
    database.do_migrations()
    # Initialise user
    user.init()

@app.get("/")
def read_root() -> Response:
    return Response("The server is running.")

app.include_router(userRouter.router)

app.include_router(languageRouter.router)

app.include_router(contextRouter.router)

app.include_router(testRouter.router)

