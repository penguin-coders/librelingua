##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from fastapi import APIRouter, Response, Depends

from classes.User import *
from services import contextService

router = APIRouter(
)

@router.get("/context")
def get_context(user_id: int = Depends(get_user_id_from_token)) -> Response:
    return contextService.get_context(user_id)

@router.get("/contextlist")
def get_context_list(user_id: int = Depends(get_user_id_from_token)) -> Response:
    return contextService.get_context_list(user_id)

@router.put("/setcontext")
def set_context(ctxt: str, user_id: int = Depends(get_user_id_from_token)) -> Response:
    return contextService.set_context(user_id, ctxt)

@router.put("/translate")
def translate_text(txt: str, ctxt: str, user_id: int = Depends(get_user_id_from_token)) -> Response:
    return contextService.translate_text(user_id, ctxt, txt)

@router.put("/addsentence")
def add_sentence(tgt_txt: str, ntv_txt: str, ctxt: str, 
                   user_id: int = Depends(get_user_id_from_token)) -> Response:
    return contextService.add_sentence(user_id, ctxt, tgt_txt, ntv_txt)
