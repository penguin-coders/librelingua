##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from fastapi import APIRouter, Response, Depends
import fastapi.security as sec
from pydantic import BaseModel
from classes.User import *

router = APIRouter(
    prefix="/user"
)

class UserReg(BaseModel):
    email: str
    passwd: str
    lang: int

@router.post("/register")
def register_user(ur: UserReg) -> Response:
    user = User(ur.email, ur.passwd, ur.lang)
    user.register()
    return user.create_token()
    
@router.post("/login")
def user_login(form_data: sec.OAuth2PasswordRequestForm = Depends()) -> Response:

    user = User(form_data.username, form_data.password)

    if not user.authenticate():
        raise HTTPException(status_code=401, detail="Invalid Credentials")

    return user.create_token()
