##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from fastapi import APIRouter, Response, Depends

from classes.User import *
from services import languageService

router = APIRouter(
)

@router.get("/language")
def get_language(user_id: int = Depends(get_user_id_from_token)) -> Response:
    return languageService.get_user_language(user_id)

@router.put("/setlang")
def set_language(lang: str, user_id: int = Depends(get_user_id_from_token)) -> Response:
    return languageService.set_user_language(user_id, lang)

