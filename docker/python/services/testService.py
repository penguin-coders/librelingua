##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from random import choice

import database
import user
import sys

from classes.User import *
from classes.Subject import Person, Number, Gender, Subject
from classes.Verb import *
from classes.Noun import *
from classes.Phrase import *
from classes.Word import *
from classes.Sentence import Sentence

def get_test_item(user_id):
    # choose a sentence at random
    sentence = choice(user.sentences[user_id])

    # randomly choose paramaters for the subject (i.e. the subject of the sentence)
    person = choice(list(Person))
    number = choice(list(Number))
    gender = choice([1,2]) if user.tgt_lang[user_id]=='fr' else choice(list(Gender)) # temporary hack for French
    sentence.set_subject(Subject(person=person, number=number, gender=gender))
    sentence.randomise()

    # get strings in both languages
    str_tgt = sentence.get_tgt_sentence()
    str_ntv = sentence.get_ntv_sentence()
    disambiguation = sentence.get_disambiguation()
    return {"question":str_ntv, "answer":str_tgt, "disambiguation":disambiguation}

def get_random_subject():
    # randomly choose paramaters for the subject (i.e. the subject of the sentence)
    person = choice(list(Person))
    number = choice(list(Number))
    gender = choice(list(Gender))
    return Subject(person=person, number=number, gender=gender)

def get_nouns(tgt_lang, ntv_lang, ntv_ids = [], fixed_props = {}):
    ntv_nouns = []
    tgt_nouns = []
    sql = "SELECT * FROM vw_nt_nouns WHERE n1_lang=%s AND n2_lang=%s"
    if(ntv_ids==[]):
        nouns = database.db.fetchall(sql, (tgt_lang, ntv_lang))
    else:
        sql += " AND lang2_id = ANY(%s)"
        print(sql, file=sys.stderr)
        nouns = database.db.fetchall(sql, (tgt_lang, ntv_lang, ntv_ids))
 
    tgt_endings = get_declensions_from_db(tgt_lang)
    ntv_endings = get_declensions_from_db(ntv_lang)
    tgt_cases = get_cases_from_db(tgt_lang)
    ntv_cases = get_cases_from_db(ntv_lang)
    all_cases = tgt_cases if len(tgt_cases)>len(ntv_cases) else ntv_cases

    for noun in nouns:
        tgt_nouns.append(Noun(
                tgt_lang, 
                noun['n1_lemma'], 
                noun['n1_declension'], 
                tgt_endings,
                tgt_cases,
                all_cases,
                noun['n1_irregularities'],
                fixed_props))
        ntv_nouns.append(Noun(
                ntv_lang, 
                noun['n2_lemma'], 
                noun['n2_declension'], 
                ntv_endings,
                ntv_cases,
                all_cases,
                noun['n2_irregularities'],
                fixed_props))
    return (ntv_nouns, tgt_nouns)

def get_verbs(tgt_lang, ntv_lang, ntv_ids = [], fixed_props = {}):
    ntv_verbs = []
    tgt_verbs = []
    sql = "SELECT * FROM vw_vt_verbs WHERE v1_lang=%s AND v2_lang=%s"
    if(ntv_ids==[]):
        verbs = database.db.fetchall(sql, (tgt_lang, ntv_lang))
    else:
        sql += " AND lang2_id = ANY(%s)"
        print(sql, file=sys.stderr)
        verbs = database.db.fetchall(sql, (tgt_lang, ntv_lang, ntv_ids))
 
    for vb in verbs:
        # get target lang endings, pronouns and regexes
        tgt_endings = get_endings_from_db(tgt_lang)
        tgt_pronouns = get_third_person_pronouns_from_db(tgt_lang)
        tgt_regexes = get_regexes_from_db(tgt_lang)
        # get native lang endings, pronouns and regexes
        ntv_endings = get_endings_from_db(ntv_lang)
        ntv_pronouns = get_third_person_pronouns_from_db(ntv_lang)
        ntv_regexes = get_regexes_from_db(ntv_lang)
        tgt_verbs.append(Verb(
                tgt_lang, 
                vb['v1_infinitive'], 
                vb['v1_conjugation'], 
                tgt_endings,
                tgt_pronouns,
                tgt_regexes,
                vb['v1_irregularities'],
                fixed_props))
        ntv_verbs.append(Verb(
                ntv_lang, 
                vb['v2_infinitive'], 
                vb['v2_conjugation'], 
                ntv_endings,
                ntv_pronouns,
                ntv_regexes,
                vb['v2_irregularities'],
                fixed_props))
    return (ntv_verbs, tgt_verbs)

def get_sentences(tgt_lang, ntv_lang, ctxt):

    sentences = []

    # get sentence patterns from db
    sentence_pats = get_sentence_pats(tgt_lang,ntv_lang, ctxt)
    
    # get phrases for each sentence pattern
    for sentence_pat in sentence_pats:
        phrases = get_phrases(sentence_pat['phrases'])

        # loop through phrases
        phrase_objects = []
        for i, phrase in phrases.items():

            # get words for phrase
            ntv_words = get_words(phrase['ntv_words'])
            tgt_words = get_words(phrase['tgt_words'])

            # add translation mappings to ntv_words and tgt_words
            for [ntv_id, tgt_id] in phrase['translations']:
                ntv_word_id = phrase['ntv_words'][ntv_id] 
                tgt_word_id = phrase['tgt_words'][tgt_id] 
                ntv_words[ntv_word_id]['trans'] = tgt_word_id
                tgt_words[tgt_word_id]['trans'] = ntv_word_id

            # get options for each word
            for idx, nw in ntv_words.items():
                match nw['pos']:
                    case 'PRON':
                        nw['options'].append(Pronouns(ntv_lang))
                        if nw['trans'] > 0:
                            tgt_words[nw['trans']]['options'].append(Pronouns(tgt_lang))
                    case 'VERB':
                        (ntv_verbs, tgt_verbs) = get_verbs(
                                tgt_lang, ntv_lang, 
                                nw['word_ids'],
                                nw['fixed_properties'])
                        nw['options'] = ntv_verbs
                        if nw['trans'] > 0:
                            tgt_words[nw['trans']]['options'] = tgt_verbs
                    case 'NOUN':
                        (ntv_nouns, tgt_nouns) = get_nouns(
                                tgt_lang, ntv_lang, 
                                nw['word_ids'],
                                nw['fixed_properties'])
                        nw['options'] = ntv_nouns
                        if nw['trans'] > 0:
                            tgt_words[nw['trans']]['options'] = tgt_nouns

            for idx, tw in ntv_words.items():
                if len(tw['options'])==0:
                    match tw['pos']:
                        case 'PRON':
                            tw['options'].append(Pronouns(tgt_lang))
                        case 'VERB':
                            (ntv_verbs, tgt_verbs) = get_verbs(
                                    tgt_lang, ntv_lang, 
                                    tw['word_ids'],
                                    tw['fixed_properties'])
                            tw['options'] = tgt_verbs

            # make object arrays
            ntv_word_objects = [Word(ntv_words[i]['pos'], ntv_words[i]['options']) for i in phrase['ntv_words']]
            tgt_word_objects = [Word(tgt_words[i]['pos'], tgt_words[i]['options']) for i in phrase['tgt_words']]

            # make Phrase object
            phrase_objects.append(Phrase(ntv_word_objects, tgt_word_objects, phrase['translations']))
            sentences.append(Sentence(Subject(), phrase_objects, sentence_pat['ntv_word_order'], sentence_pat['tgt_word_order'], sentence_pat['disambiguations']))

    return sentences

def get_sentence_pats(tgt_lang, ntv_lang, ctxt):
    sql = """
        SELECT s.* AS sentence_id
        FROM contexts c 
        JOIN sentences s ON s.context_id = c.id
        JOIN languages ntv ON ntv.id = c.ntv_lang_id 
        JOIN languages tgt ON tgt.id = c.tgt_lang_id 
        WHERE tgt.code = %s AND ntv.code = %s
        """
    sql = """
        SELECT s.* AS sentence_id
        FROM contexts c 
        JOIN sentences s ON s.context_id = c.id
        WHERE c.id = %s 
        """
    print(f"ctxt: {ctxt}")
    #return database.db.fetchone(sql, (tgt_lang, ntv_lang))
    return database.db.fetchall(sql, (ctxt,))

def get_phrases(phraseIDs):
    sql = "SELECT * FROM phrases WHERE id = ANY(%s)"
    return { p['id']: p for p in database.db.fetchall(sql, (phraseIDs,)) }

def get_words(wordIDs):
    sql = """
        SELECT w.*, pos.pos, -1 AS trans, ARRAY[]::integer[] AS options 
        FROM words w
        JOIN parts_of_speech pos ON pos.id = w.part_of_speech
        WHERE w.id = ANY(%s)
        """
    return { p['id']: p for p in database.db.fetchall(sql, (wordIDs,)) }

