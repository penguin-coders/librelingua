##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from classes.User import *
import database
import user
from services import contextService, testService

def get_user_language(user_id: int):
    user_lang = _get_user_language_from_db(user_id)
    user_ctxt = contextService.get_context_for_user_lang(user_id, user_lang['id'])
    user.sentences[user_id] = []
    user.sentences[user_id] += testService.get_sentences(user_lang['code'], 'en', user_ctxt['context_id'])
    user.tgt_lang[user_id] = user_lang['code']
    return user_lang

def set_user_language(user_id: int, lang: str):
    user_lang = _update_user_language_in_db(user_id, lang)
    user_ctxt = contextService.get_context_for_user_lang(user_id, user_lang['id'])
    user.sentences[user_id] = []
    user.sentences[user_id] += testService.get_sentences(user_lang['code'], 'en', user_ctxt['context_id'])
    user.tgt_lang[user_id] = user_lang['code']
    return user_lang

def _get_user_language_from_db(user_id):
    query = """
            SELECT ul.user_id, l.id, l.name, l.code FROM user_languages ul
                JOIN languages l ON l.id = ul.tgt_lang_id
                WHERE ul.user_id = %s
            """
    return database.db.fetchone(query, (user_id,))

def _update_user_language_in_db(user_id, lang_id):
    query = """
            UPDATE user_languages SET tgt_lang_id = %s 
                WHERE user_id = %s
            """
    database.db.update(query, (lang_id, user_id))
    return get_user_language(user_id)
