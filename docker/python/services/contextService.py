##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from classes.User import *
import database
import user
from services import languageService, testService, translationService, importService

def get_context(user_id):
    user_lang = languageService.get_user_language(user_id)
    return get_context_for_user_lang(user_id, user_lang['id'])

def get_context_for_user_lang(user_id, user_lang_id):
    return _get_user_ctxt(user_id, user_lang_id)

def get_context_list(user_id):
    user_lang = languageService.get_user_language(user_id)
    return _get_contexts(user_lang['id'])

def set_context(user_id, ctxt):
    user_lang = languageService.get_user_language(user_id)
    _update_user_context(user_id, user_lang['id'], ctxt)
    user.sentences[user_id] = []
    user.sentences[user_id] += testService.get_sentences(user_lang['code'], 'en', ctxt)
    return _get_user_ctxt(user_id, user_lang['id'])

def translate_text(user_id, ctxt, txt):
    langs =  _get_languages_for_context(ctxt)
    return translationService.translate(langs['tgt_lang'], langs['ntv_lang'], txt)

def add_sentence(user_id, ctxt, tgt_txt, ntv_txt):
    langs =  _get_languages_for_context(ctxt)
    tgt_tokens = translationService.get_stanza_tokens(langs['tgt_lang'], tgt_txt)
    ntv_tokens = translationService.get_stanza_tokens(langs['ntv_lang'], ntv_txt)
    sentence_ids = importService.create_sentences_from_tokens(
        ctxt, langs['ntv_lang_id'], langs['tgt_lang_id'], ntv_tokens, tgt_tokens)
    return sentence_ids

def _update_user_context(user_id, lang_id, ctxt_id):
    query = """
            UPDATE user_contexts SET context_id = %s
            WHERE tgt_lang_id = %s 
            AND user_id = %s
            """
    database.db.update(query, (ctxt_id, lang_id, user_id))
    return ctxt_id

def _get_user_ctxt(user_id, lang):
    sql = """
        SELECT * FROM contexts c 
        JOIN user_contexts uc ON uc.context_id = c.id
        WHERE uc.tgt_lang_id = %s
        AND uc.user_id = %s
        """
    return database.db.fetchone(sql, (lang, user_id,)) 

def _get_contexts(lang):
    sql = """
        SELECT * FROM contexts c 
        WHERE tgt_lang_id = %s
        """
    return database.db.fetchall(sql, (lang,)) 

def _get_languages_for_context(context_id):
    sql = """
        SELECT c.id, ntv.code AS ntv_lang, tgt.code AS tgt_lang, c.ntv_lang_id, c.tgt_lang_id
        FROM contexts c
        JOIN languages ntv ON ntv.id=c.ntv_lang_id
        JOIN languages tgt ON tgt.id=c.tgt_lang_id
        WHERE c.id = %s;
        """
    return database.db.fetchone(sql, (context_id,)) 

