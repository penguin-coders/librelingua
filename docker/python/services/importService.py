##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

from classes.User import *
import json
import database
import user
import sys

def create_sentences_from_tokens(ctxt_id, ntv_lang_id, tgt_lang_id, ntv_tokens, tgt_tokens):

    for ntv_sent, tgt_sent in zip(ntv_tokens.sentences, tgt_tokens.sentences):
        # add words
        ntv_words = _add_words(ntv_lang_id, ntv_sent.words)
        tgt_words = _add_words(tgt_lang_id, tgt_sent.words)

        # add phrase
        translations = _get_translations(ntv_words, tgt_words)
        ntv_word_ids = [ w.word_id for w in ntv_words ]
        tgt_word_ids = [ w.word_id for w in tgt_words ]
        phrase_ids =  [ _add_phrase(ntv_word_ids, tgt_word_ids, translations) ]

        # add sentence
        disambiguations = {}
        ntv_word_order = [[0,idx] for idx, id in enumerate(ntv_word_ids)]
        tgt_word_order = [[0,idx] for idx, id in enumerate(tgt_word_ids)]
        return _add_sentence(ctxt_id, phrase_ids, ntv_word_order, tgt_word_order, disambiguations)

def _add_words(lang_id, words):
    for i, word in enumerate(words):
        pos = _get_pos(word.upos)
        word.pos_id = pos['id']
        word.database_id = _get_id_for_word(pos['table_name'], lang_id, word.lemma)
        word_ids = [ word.database_id ]
        fixed_props = _get_fixed_props(word.feats)
        sql = """
            INSERT INTO words (part_of_speech, word_ids, lang, fixed_properties) 
            VALUES(%s, %s, %s, %s) 
            RETURNING id;
            """
        word.word_id = database.db.insert(sql, 
                        (pos['id'], json.dumps(word_ids), lang_id, json.dumps(fixed_props)))
        words[i] = word
    return words

def _add_phrase(ntv_word_ids, tgt_word_ids, translations):
    sql = """
        INSERT INTO phrases (tgt_words, ntv_words, translations) 
        VALUES(%s, %s, %s) 
        RETURNING id;
        """
    return database.db.insert(sql, 
            (json.dumps(tgt_word_ids), json.dumps(ntv_word_ids), json.dumps(translations)))

def _add_sentence(ctxt_id, phrase_ids, ntv_word_order, tgt_word_order, disambiguations):
    sql = """
        INSERT INTO sentences 
        (ntv_word_order, tgt_word_order, context_id, phrases, disambiguations) 
        VALUES(%s, %s, %s, %s, %s) 
        RETURNING id;
        """
    return database.db.insert(sql, 
            (json.dumps(ntv_word_order), json.dumps(tgt_word_order), 
            ctxt_id, json.dumps(phrase_ids), json.dumps(disambiguations)))
    
def _get_translations(ntv_words, tgt_words):

    translations = []
    ntv_indexes = { word.word_id: idx for idx, word in enumerate(ntv_words) }
    tgt_indexes = { word.word_id: idx for idx, word in enumerate(tgt_words) }

    print("DBG: "+str(ntv_indexes), file=sys.stderr)
    print("DBG: "+str(tgt_indexes), file=sys.stderr)

    for (pos, table_name) in [('NOUN','vw_nt_nouns'),('VERB','vw_vt_verbs')]:

        sql = """
            SELECT lang1_id, lang2_id FROM {table}
            WHERE lang1_id = ANY(%s)
            AND lang2_id = ANY(%s)
            """.format(table = table_name)

        ntv_words_filtered = [ w for w in ntv_words if w.pos==pos ]
        tgt_words_filtered = [ w for w in tgt_words if w.pos==pos ]
        
        ntv_db_ids = [ w.database_id for w in ntv_words_filtered ]
        tgt_db_ids = [ w.database_id for w in tgt_words_filtered ]

        results = database.db.fetchall(sql, (ntv_db_ids, tgt_db_ids))

        for res in results:
            ntv_word_ids = [ 
                    w.word_id for w in ntv_words_filtered if w.database_id==res['lang1_id']]
            tgt_word_ids = [ 
                    w.word_id for w in tgt_words_filtered if w.database_id==res['lang2_id']]
            translations += [ 
                    [ ntv_indexes[nw_id], tgt_indexes[tw_id] ] 
                    for nw_id, tw_id in zip(ntv_word_ids, tgt_word_ids) ]
            
            print("DBG: ntv_word_ids: "+str(ntv_word_ids), file=sys.stderr)
            print("DBG: tgt_word_ids: "+str(tgt_word_ids), file=sys.stderr)
            print("DBG: translations: "+str(translations), file=sys.stderr)

    return translations
        
def _get_pos(upos):
    sql = """
        SELECT id, table_name FROM parts_of_speech 
        WHERE pos=%s
        """
    return database.db.fetchone(sql, (upos,))

def _get_id_for_word(table_name, lang_id, lemma):

    match table_name:
        case 'nouns':
            field_name = 'lemma'
        case 'verbs':
            field_name = 'infinitive'
        case 'pronouns':
            field_name = 'pronoun'
        case _:
            return -1
    
    sql = """
        SELECT id FROM {table} WHERE {field} = %s 
        AND language_id = %s;
        """.format(table=table_name, field=field_name)
    res = database.db.fetchone(sql, (lemma, lang_id))
    return res['id']

def _get_fixed_props(feats):
    fixed_props = {}
    if feats is None:
        return fixed_props 
    features = {}
    for feature in feats.split("|"):
        key, val = feature.split("=")
        features[key.lower()] = val.lower()
    if "case" in features.keys():
        fixed_props["case"] = features["case"]
        fixed_props["number"] = 2 if features["number"]=="plur" else 1
        return fixed_props
    if "verbform" in features.keys() and features["verbform"]=="inf":
        fixed_props["verb_form"] = "infinitive"
    return fixed_props

