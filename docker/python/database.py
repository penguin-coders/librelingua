##########################################################################
# LibreLinguist - a free and open source language learning tool          #
# Copyright (C) 2022-2024 Mark Williams                                  #
#                                                                        #
# This file is part of LibreLinguist.                                    #
#                                                                        #
# LibreLinguist is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# LibreLinguist is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public licenses     #
# along with LibreLinguist. If not, see <https://www.gnu.org/licenses/>. #
##########################################################################

import psycopg
from psycopg.rows import dict_row
from config import get_db
from yoyo import read_migrations
from yoyo import get_backend

db = ''

class DataBase:

    def __init__(self, db_config):
        self.db_config = db_config
        self.conn = psycopg.connect(**db_config)
        self.cur = self.conn.cursor(row_factory=dict_row)

    def geturl(self):
        return f"postgresql+psycopg://{self.db_config['user']}:{self.db_config['password']}@{self.db_config['host']}/{self.db_config['dbname']}"

    def fetchall(self, *args):
        self.cur.execute(*args)
        results = self.cur.fetchall()
        return results
    
    def fetchone(self, *args):
        self.cur.execute(*args)
        results = self.cur.fetchone()
        return results

    def insert(self, *args):
        self.cur.execute(*args)
        try:
            id = self.cur.fetchone()['id']
        except:
            id = -1
        self.cur.execute("COMMIT")
        return id
    
    def update(self, *args):
        self.cur.execute(*args)
        self.cur.execute("COMMIT")

########## End of class definition ##############

def init_db():
    global db
    db = DataBase(get_db())

def do_migrations():    
    # Apply any outstanding migrations
    backend = get_backend(db.geturl())
    migrations = read_migrations('./migrations')
    with backend.lock():
        backend.apply_migrations(backend.to_apply(migrations))
