## v.0.8.0 - 2024-07-11

Added endpoint and importService to import sentences from stanza tokens

**Added:**
- addsentence endpoint
- importService
- function to generate stanza tokens

## v.0.7.0 - 2024-07-10
Added translate endpoint to API, which returns a translation of the text submitted, translated by argostranslate.

**Added:**
- translate endpoint 
- translationService

## v.0.6.0 - 2024-06-29

Added argostranslate and switched torch version

**Added:**
- argostranslate

**Changed:**
- Switched to CPU-only version of torch
- Stanza now using older version compatible with argostranslate

## v.0.5.0 - 2024-06-28

Added stanza to python docker container

**Added:**
- Stanza

## v.0.4.2 - 2024-06-28

Added disambiguations for nouns, which can be enabled in the database

**Added:**
- Noun disambiguations

## v.0.4.1 - 2024-06-25

Database corrections

**Fixed:**
- View for noun translations
- Noun translation table entry for banana
- Incorrect sequence IDs

## v.0.4.0 - 2024-06-24

Added support for nouns and verb infinitives

**Added:**
- Nouns
- Verb infinitives
- Filtering functionality based on word\_ids field in words table 
- Functionality to fix properties of a word via fixed\_properties field in words table

**Changed:**
- View for verb translations to include IDs, so that tests can filter on IDs
- Polish verbs test to include infinitives

## v.0.3.2 - 2024-03-02

Refactored API into routes and services

**Changed:**
- main.py refactored and split into multiple files

## v.0.3.1 - 2024-02-24

Refactored database connection code to maintain a connection, rather than creating a new connection for each query. Also renamed webapi.py to main.py

**Changed:**
- Database connection code
- webapi renamed to main

## v.0.3.0 - 2024-02-11

Added context switcher

**Added:**
- Database table to store selected user context
- Context switcher

## v.0.2.9 - 2024-02-03

Pulled through context name from db for use as test title

**Changed:**
- h2 for title of test

## v.0.2.8 - 2024-02-01

Further refactoring to define Sentence, Phrase, and Word relationships in the database.

**Added:**
- New database tables contexts, sentences, phrases, words, parts\_of\_speech

**Changed:**
- Replaced hard-wired sentences with code to retreive from database

## v.0.2.7 - 2024-01-28

Refactored code in preparation for v.0.3.0. Only visible change is some new verbs.

**Added:**
- Phrase and Word classes
- 3 new Polish verbs

**Changed:**
- Sentence objects now made up of Phrases which comprise Words
- Refactored verb conjugation to use pronoun IDs instead of actual pronouns
- Renamed Actor to Subject
- Some refactoring to use dependency injection

## v.0.2.6 - 2023-03-04

Change to name of project, and a few bug fixes

**Changed:**
- Name of project changed to LibreLinguist

**Fixed:**
- Test now updates after language switch 
- Minor bug in Loading.js JSX

## v.0.2.5 - 2023-03-01

Added loading spinner to fix a few timing issues

**Added:**
- Loading spinner

**Fixed:**
- Radio button timing issue
- Timing issues with loading test after login/register or language switch

## v.0.2.4 - 2023-02-27

Fixed bug where test was not updating after a language switch

**Fixed:**
- Test now updates after language switch

## v.0.2.3 - 2023-02-27

Added registration form and refactored login form

**Added:**
- Registration form

**Changed:**
- Refactored login form
- Conditionalised header controls

## v.0.2.2 - 2023-02-26

Added front-end language switcher.

**Added:**
- Front-end language switcher

**Removed:**
- Proxy containter

## v.0.2.1 - 2023-02-25

Added user accounts with jwt tokens to support different languages for different users.

**Added:**
- User account functionality
- Login form

**Changed:**
- Verb test now uses logged in user's language settings

## v.0.2.0 - 2023-02-18

Eliminated language specific code by moving more things to the database and added support for French verbs.

**Added:**
- French verb support
- A few French verbs

**Changed:**
- Language specific code made generic

## v.0.1.0 - 2023-02-12

Backend database added

**Added:**
- PostgreSQL container

**Changed:**
- Data moved from code to database

## v.0.0.2 - 2023-02-07

Web version with React front-end

**Added:**
- Code for React front-end
- Apache container to add CORS header

**Changed:**
- Python code containerised

## v.0.0.1 - 2022-12-30

First usable version

**Added:**
- Unit tests
- Code to represent a handful of verbs in Polish and English
- Code to test user on translating verbs from English to Polish
